﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace TrivuelosEntities
{
    public class EUsuario
    {
        public int Id { get; set; }
        public string  Nombre{ get; set; }
        public string Apellido{ get; set; }
        public string  Correo { get; set; }
        public Image Imagen { get; set; }
    }
}
