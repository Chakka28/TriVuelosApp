﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trivuelosentities
{
    public class EAerolinea
    {
        public int Id { get; set; }
        public string Fs { get; set; }
        public string Iata { get; set; }
        public string Icao { get; set; }
        public string Nombre { get; set; }
        public bool Activo { get; set; }
    }
}
