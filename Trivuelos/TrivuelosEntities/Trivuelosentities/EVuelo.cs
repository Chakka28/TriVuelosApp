﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trivuelosentities
{
    public class EVuelo
    {
        public int Id { get; set; }
        public string LlegadaAeropuerto { get; set; }
        public string HoraLlegada { get; set; }
        public string Portador { get; set; }
        public string SalidaAeropuerto { get; set; }
        public string HoraSalida { get; set; }
        public string VueloEquipo { get; set; }
        public string NumeroVuelo { get; set; }
        public int Parada { get; set; }

    }
}
