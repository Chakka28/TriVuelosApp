﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trivuelosentities
{
    public class EAeropuerto
    {
        public int Id { get; set; }
        public string Fs { get; set; }
        public string Faa { get; set; }
        public string Iata { get; set; }
        public string Icao { get; set; }
        public string Nombre { get; set; }
        public string Cuidad { get; set; }
        public string CodigoEstado { get; set; }
        public string CodigoPais { get; set; }
        public string NombrePais { get; set; }
        public string NombreRegion { get; set; }
        public string TiempoZonaRegion { get; set; }
        public double Latitud { get; set; }
        public double Longitud { get; set; }
        public int ElevacionPie { get; set; }
        public bool Activo { get; set; }
        public int Clasificacion { get; set; }
        public DateTime Tiempo { get; set; }
    }
}
