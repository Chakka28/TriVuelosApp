﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trivuelosentities
{
    public class EPosicion
    {
        public double Logitud { get; set; }
        public double Latitud { get; set; }
        public int Velocidad { get; set; }
        public string Fuente { get; set; }
    }
}
