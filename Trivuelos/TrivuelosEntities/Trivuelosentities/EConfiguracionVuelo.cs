﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trivuelosentities
{
    public class EConfiguracionVuelo
    {
        public long Id { get; set; }
        public string Aeropuerto { get; set; }
        public string Aeropuerto2 { get; set; }
        public string Aerolinea { get; set; }
        public string NumeroVuelo { get; set; }
        public int Dia { get; set; }
        public int Mes { get; set; }
        public int Anno { get; set; }
        public int Hora { get; set; }

    }
}
