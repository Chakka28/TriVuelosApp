﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trivuelosentities;
using TrivuelosDAL;
namespace TrivuelosBOL
{
    public class GoogleBOL
    {

        private GoogleDAL gdal;
        private UsuarioBOL ubol;

        public GoogleBOL()
        {
            gdal = new GoogleDAL();
            ubol = new UsuarioBOL();

        }


        /// <summary>
        /// Obtiene un usuario de Google plus
        /// </summary>
        /// <returns>Usuario</returns>
        public EUsuario ObtenerUsuario()
        {
            EUsuario usuario = gdal.DatosUsuario();
            usuario = ubol.verificacion(usuario);
            if (usuario.Id == 0)
            {
                throw new Exception("El usuario no acepto los terminos");
            }
            return usuario;
        }
    }
}
