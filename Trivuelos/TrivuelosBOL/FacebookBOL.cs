﻿using Facebook;
using Facebook.MiniJSON;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Trivuelosentities;

namespace TrivuelosBOL
{
    public class FacebookBOL
    {
        private const string AppId = "248708062415156";
        private const string ExtendedPermissions = "public_profile,email";
        private string _accessToken;
        private AnalyzeBOL fbAnalyze;
        private UsuarioBOL ubo;

        public FacebookBOL(AnalyzeBOL fbAnalyze)
        {
            this.fbAnalyze = fbAnalyze;
            ubo = new UsuarioBOL();
        }

        public FacebookBOL()
        {
        }

       
       

        /// <summary>
        /// Obtiene el usuario 
        /// </summary>
        /// <param name="accessToken">tooken de facebook </param>
        /// <returns>dynamic MyData</returns>
        private object Obtener_Usuario(string accessToken)
        {
            FacebookClient fb = new FacebookClient(accessToken);
            dynamic MyData = fb.Get("/me");
            return MyData.id;
        }


        /// <summary>
        /// Convierte la imagen 
        /// </summary>
        /// <param name="url">url de la imagen </param>
        /// <returns></returns>
        public Image ExtraerImagen(string url)
        {
            WebClient wc = new WebClient();
            byte[] bytes = wc.DownloadData(url);
            MemoryStream ms = new MemoryStream(bytes);
            return Image.FromStream(ms);
        }



    }
}
