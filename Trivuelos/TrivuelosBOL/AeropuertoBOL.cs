﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrivuelosDAL;
using Trivuelosentities;

namespace TrivuelosBOL
{
    public class AeropuertoBOL
    {
        private AeropuertoDAL apdal;

        public AeropuertoBOL()
        {
            apdal = new AeropuertoDAL();
        }

        /// <summary>
        /// Actualiza los aeropuertos de la base de datos
        /// </summary>
        public void Actualizar()
        {
            apdal.ActualizarDatos();
        }

        /// <summary>
        /// Carga todo los aeropuertos de la base de datos
        /// </summary>
        /// <returns>Lista de aeropuertos</returns>
        public List<EAeropuerto> CargarDatos(EUsuario u )
        {
            return apdal.Extraer(u);
        }

        /// <summary>
        /// Elimina el aeropuerto que seleccione el usuario
        /// </summary>
        /// <param name="aeropuerto"></param>
        /// <param name="usuario"></param>
        public void  EliminarPorUsuario(EAeropuerto aeropuerto, EUsuario usuario)
        {
            apdal.EliminarAeroPorUsuario(aeropuerto, usuario);
        }

    }
}
