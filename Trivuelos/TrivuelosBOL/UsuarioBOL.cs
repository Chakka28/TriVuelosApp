﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrivuelosDAL;
using Trivuelosentities;

namespace TrivuelosBOL
{
    public class UsuarioBOL
    {
        private UsuarioDAL udal;

        public UsuarioBOL()
        {
            udal = new UsuarioDAL();
        }



        /// <summary>
        /// Verifica al usuario en el caso de que el usuario no exista 
        /// retona un false lo que conlleva a agregarlo a la base de datos 
        /// </summary>
        /// <param name="u">usuario a verificar</param>
        public EUsuario verificacion(EUsuario u)
        {
            
            if(udal.verificar(u) == 0)
            {
               u.Id = udal.Insertar(u);
                return u;
            }
            else
            {
                u.Id = udal.verificar(u);
                return u;
            }
        }
    }
}
