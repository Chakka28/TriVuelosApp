﻿using Facebook;
using Facebook.MiniJSON;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrivuelosBOL
{
    public class AnalyzeBOL
    {
        protected readonly FacebookClient _fb;

        private string id;
        private string name;
        private string email;

        public AnalyzeBOL()
        {
        }


        public AnalyzeBOL(FacebookClient fb)
        {
            if (fb == null)
                throw new ArgumentNullException("fb");

            _fb = fb;

        }


        /// <summary>
        /// Saca los datos json
        /// </summary>
        public void Sacar()
        {
            var result = _fb.Get("me", new { fields = new[] { "id", "name", "email" } });


            var dict = Json.Deserialize(result.ToString()) as Dictionary<string, object>;


            id = dict["id"].ToString();
            name = dict["name"].ToString();
            email = dict["email"].ToString();
            ///  dgUsu.Rows.Add(name, email, id);
            ///  Hay que sacar los datos y mandarlos a guardar
        }

    }
}

