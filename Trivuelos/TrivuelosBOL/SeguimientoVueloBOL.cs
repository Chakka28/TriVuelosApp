﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrivuelosDAL;
using Trivuelosentities;
using TrivuelosDAL.WSVueloSeguimiento;
namespace TrivuelosBOL
{
    public class SeguimientoVueloBOL
    {
        private SeguimientoVueloDAL sdal;

        public SeguimientoVueloBOL()
        {
            sdal = new SeguimientoVueloDAL();
        }

        /// <summary>
        /// Valida si los datos que digita el usuario son númericos
        /// </summary>
        /// <param name="text"></param>
        /// <returns>true si son números, false si no lo son</returns>
        public long Valido(string text)
        {
            if (String.IsNullOrEmpty(text))

            {
                throw new Exception("Datos requeridos");
            }

            if (!long.TryParse(text, out long val))
            {
                throw new Exception("Digite valores numericos");
            }
            return val;
        }

        /// <summary>
        /// Cargar los datos de un vuelo por id
        /// </summary>
        /// <param name="vuelo"></param>
        public void CargarVueloId(long vuelo)
        {
            if (vuelo <= 0)
            {
                throw new Exception("Tiene que escoger un vuelo");
            }

            sdal.CargarVueloId(vuelo);
        }

        /// <summary>
        /// Carga un arreglo de puntos del vuelo para una ruta
        /// </summary>
        /// <returns>Arreglo de puntos</returns>
        public EPunto[] CargarPuntos(long id)
        {
            return sdal.ExtraerPuntosVuelo(id);
        }


        /// <summary>
        /// Cargar un arreglo de posiciones del vuelo
        /// </summary>
        /// <returns>Arerglo de posiciones</returns>
        public EPosicion[] CargarPosiciones(long id)
        {
            return sdal.ExtraerPosicionVuelo(id);
        }
        /// <summary>
        /// Carga un arreglo de puntos del vuelo para una ruta
        /// </summary>
        /// <returns>Arreglo de puntos</returns>
        public EPunto[] CargarPuntosFecha(EConfiguracionVuelo con)
        {
            EPunto[] punto = sdal.ExtraerPuntosVueloFecha(con) ?? null;
            if (punto == null)
            {
                throw new Exception("Ese vuelo esta fuera de servicio");
            }
            return punto;
        }


        /// <summary>
        /// Cargar un arreglo de posiciones del vuelo
        /// </summary>
        /// <returns>Arerglo de posiciones</returns>
        public EPosicion[] CargarPosicionesFecha(EConfiguracionVuelo con)
        {
            EPosicion[] posiciones = sdal.ExtraerPosicionesVueloFecha(con) ?? null;
            if (posiciones == null)
            {
                throw new Exception("El avión aun no ha despegado");
            }
            return posiciones;
        }

        /// <summary>
        /// Cargar los datos de configuración de un vuelo
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public EConfiguracionVuelo CargarConfiguracion(long id)
        {
            return sdal.ConfiguracionVuelo(id);
        }
    }
}
