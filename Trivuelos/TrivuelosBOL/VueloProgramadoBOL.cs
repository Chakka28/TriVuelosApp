﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trivuelosentities;
using TrivuelosDAL;
namespace TrivuelosBOL
{
    public class VueloProgramadoBOL
    {
        private VuelosProgramadosDAL vpdal;

        public VueloProgramadoBOL()
        {
            vpdal = new VuelosProgramadosDAL();
        }

        /// <summary>
        /// Cargar los vuelos de salida de un aeropuerto
        /// </summary>
        /// <param name="con"></param>
        /// <returns>Lista de vuelos</returns>
        public List<EVuelo> ObtenerVuelosAeropuertoSalida(EConfiguracionVuelo con)
        {
            if (String.IsNullOrEmpty(con.Aeropuerto))
            {
                throw new Exception("Debe seleccionar un aeropuerto");
            }

            List<EVuelo> vuelos = vpdal.ExtraerSalidaFechaAeropuerto(con) ?? null;
            if (vuelos == null)
            {
                throw new Exception(String.Format("El aeropuerto {0} no tiene vuelos, intente más tarde o con otro aeropuerto", con.Aeropuerto));
            }
            return vuelos;
        }

        /// <summary>
        /// Cargar los vuelos de llegada de un aeropuerto
        /// </summary>
        /// <param name="con"></param>
        /// <returns>Lista de vuelos</returns>
        public List<EVuelo> ObtenerVuelosAeropuertoLlegada(EConfiguracionVuelo con)
        {
            if (String.IsNullOrEmpty(con.Aeropuerto))
            {
                throw new Exception("Debe seleccionar un aeropuerto");
            }
            List<EVuelo> vuelos = vpdal.ExtraerLlegadaFechaAeropuerto(con) ?? null;
            if (vuelos == null)
            {
                throw new Exception(String.Format("El aeropuerto {0} no tiene vuelos, intente más tarde o con otro aeropuerto", con.Aeropuerto));
            }
            return vuelos;
        }

        /// <summary>
        /// Cargar los vuelos de salida de una aerolínea
        /// </summary>
        /// <param name="con"></param>
        /// <returns>Lista de vuelos</returns>
        public List<EVuelo> ObtenerVuelosAerolineaSalida(EConfiguracionVuelo con)
        {
            if (String.IsNullOrEmpty(con.Aerolinea) || String.IsNullOrEmpty(con.NumeroVuelo))
            {
                throw new Exception("Datos requeridos!!");
            }
            if (!Int32.TryParse(con.NumeroVuelo, out int val))
            {
                throw new Exception("Use valores números, para el número de vuelo");
            }
            List<EVuelo> vuelos = vpdal.ExtraerSalidaFechaAerolinea(con) ?? null;
            if (vuelos == null)
            {
                throw new Exception(String.Format("La aerolínea {0} no tiene vuelos, intente más tarde o con otra aerolínea", con.Aeropuerto));
            }
            return vuelos;
        }

        /// <summary>
        /// Cargar los vuelos de llegada de una aerolínea
        /// </summary>
        /// <param name="con"></param>
        /// <returns>Lista de vuelos</returns>
        public List<EVuelo> ObtenerVuelosAerolineaLlegada(EConfiguracionVuelo con)
        {
            if (String.IsNullOrEmpty(con.Aerolinea) || String.IsNullOrEmpty(con.NumeroVuelo))
            {
                throw new Exception("Datos requeridos!!");
            }
            if (!Int32.TryParse(con.NumeroVuelo, out int val))
            {
                throw new Exception("Use valores números, para el número de vuelo");
            }
            List<EVuelo> vuelos = vpdal.ExtraerLlegadaFechaAerolinea(con) ?? null;
            if (vuelos == null)
            {
                throw new Exception(String.Format("El aerolínea {0} no tiene vuelos, intente más tarde o con otra aerolínea", con.Aeropuerto));
            }
            return vuelos;
        }

        /// <summary>
        /// Cargar los vuelos de llegada entre dos aeropuertos
        /// </summary>
        /// <param name="con"></param>
        /// <returns>Lista de vuelos</returns>
        public List<EVuelo> ObtenerVuelosEntreAeropuerto(EConfiguracionVuelo con)
        {
            if (String.IsNullOrEmpty(con.Aeropuerto))
            {
                throw new Exception("Debe seleccionar un aeropuerto");
            }
            if (con.Aeropuerto.Equals(con.Aeropuerto2)|| con.Aeropuerto2.Equals(con.Aeropuerto))
            {
                throw new Exception("No se puede seleccionar el mismo aeropuerto");
            }
            List<EVuelo> vuelos = vpdal.ExtraerLlegadaDosAeropuertos(con) ?? null;
            if (vuelos == null)
            {
                throw new Exception(String.Format("El aeropuerto {0} no tiene vuelos, intente más tarde o con otro aeropuerto", con.Aeropuerto));
            }
            return vuelos;
        }
    }
}
