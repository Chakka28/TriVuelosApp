﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrivuelosDAL;
using Trivuelosentities;
namespace TrivuelosBOL
{
    public class AerolineaBOL
    {
        private AerolineaDAL aldal;

        public AerolineaBOL()
        {
            aldal = new AerolineaDAL();
        }

        /// <summary>
        /// Actualiza las aerolineas de la base de datos
        /// </summary>
        public void Actualizar()
        {
            aldal.ActualizarDatos();
        }

        /// <summary>
        /// Cargar todas las aerolíneas disponibles en la base de datos
        /// </summary>
        /// <returns>Lista de aerolíneas</returns>
        public List<EAerolinea> CargarDatos(EUsuario u )
        {
            return aldal.Extraer(u);
        }

        /// <summary>
        /// Elimina una aerolímea que seleccione el usuario
        /// </summary>
        /// <param name="ae"> aerolínea</param>
        /// <param name="usuario">usuario</param>
        public void EliminarPorUsuario(EAerolinea ae, EUsuario usuario)
        {
            aldal.EliminarAeroPorUsuario(ae, usuario);
        }

    }
}
