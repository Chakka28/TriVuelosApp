﻿using Facebook;
using Facebook.MiniJSON;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrivuelosBOL;
using Trivuelosentities;

namespace Trivuelos
{
    public partial class Login : Form
    {
        private string _accessToken;
        private const string AppId = "248708062415156";
        private const string ExtendedPermissions = "public_profile,email";
        private AnalyzeBOL fbAnalyze;
        private GoogleBOL gbol;
        private FacebookBOL f;
        private UsuarioBOL ubo;


        public Login()
        {
            InitializeComponent();
            this.CenterToParent();
        }

        private void Login_Load(object sender, EventArgs e)
        {

            gbol = new GoogleBOL();
            fbAnalyze = new AnalyzeBOL();
            f = new FacebookBOL();
            ubo = new UsuarioBOL();
        }

        private void IniciarSesionGoogle(object sender, EventArgs e)
        {
            try
            {
                Hide();
                new FrmPrincipal() { Usuario = gbol.ObtenerUsuario() }.ShowDialog();
                Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Show();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var FrmL = new Facebook(AppId, ExtendedPermissions); ;
            FrmL.ShowDialog();
            DisplayAppropriateMessage(FrmL.FacebookOAuthResult);


        }



        /// <summary>
        /// Valida los results de facebook 
        /// </summary>
        /// <param name="facebookOAuthResult">FacebookOAuthResult</param>
        /// <returns>FacebookClient</returns>
        public void DisplayAppropriateMessage(FacebookOAuthResult facebookOAuthResult)
        {
            if (facebookOAuthResult != null)
            {
                if (facebookOAuthResult.IsSuccess)
                {
                    _accessToken = facebookOAuthResult.AccessToken;
                    var fb = new FacebookClient(facebookOAuthResult.AccessToken);
                    fbAnalyze = new AnalyzeBOL(fb);
                    string imgURL = String.Format("https://graph.facebook.com/{0}/picture", Obtener_Usuario(_accessToken));


                    CargarUsuario(fb);

                }
                else
                {
                    throw new Exception(facebookOAuthResult.ErrorDescription);
                }
            }
            else
            {
                Cerrar();

            }
        }

        /// <summary>
        /// Obtiene el usuario 
        /// </summary>
        /// <param name="accessToken">tooken de facebook </param>
        /// <returns>dynamic MyData</returns>
        private object Obtener_Usuario(string accessToken)
        {
            FacebookClient fb = new FacebookClient(accessToken);
            dynamic MyData = fb.Get("/me");
            return MyData.id;
        }



        /// <summary>
        /// Cierra el lokeen de facebook 
        /// </summary>
        private void Cerrar()
        {
            var webBrowser = new WebBrowser();
            var fb = new FacebookClient();
            var logouUrl = fb.GetLogoutUrl(new { access_token = _accessToken, next = "https://www.facebook.com/connect/login_success.html" });
            webBrowser.Navigate(logouUrl);

        }


        /// <summary>
        /// Carga el Uusuario de facebook 
        /// </summary>
        /// <param name="fb">FacebookClient</param>
        public void CargarUsuario(FacebookClient fb)
        {
            var MyData = fb.Get("me", new { fields = new[] { "id", "email", "first_name", "last_name" } });
            var dict = Json.Deserialize(MyData.ToString()) as Dictionary<string, object>;
            string id_Usu = dict["id"].ToString();
            string img = String.Format("https://graph.facebook.com/{0}/picture", id_Usu);

            EUsuario Usuario = new EUsuario
            {
                Nombre = dict["first_name"].ToString(),
                Apellido = dict["last_name"].ToString(),
                Correo = dict["email"].ToString(),
                Foto = f.ExtraerImagen(img),
                Activo = true
            };

            Usuario = ubo.verificacion(Usuario);
            Cerrar();
            Hide();
            new FrmPrincipal() { Usuario = Usuario }.ShowDialog();
            Show();



        }





    }
}
