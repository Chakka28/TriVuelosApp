﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Trivuelos.WSAeropuerto;
using TrivuelosBOL;
using Trivuelosentities;

namespace Trivuelos
{
    public partial class Prueba : Form
    {
        private AeropuertoBOL apbol;
        private AerolineaBOL albol;
        internal EUsuario Usuario { get; set; }

        
        private bool activo;
        private bool activo2;
        private ThreadStart delegado;
        private Thread hilo;
        private ThreadStart delegado2;
        private Thread hilo2;

        public Prueba()
        {
            InitializeComponent();
        }

        private void Probar(object sender, EventArgs e)
        {

        }

        private void Prueba_Load(object sender, EventArgs e)
        {
            apbol = new AeropuertoBOL();
            albol = new AerolineaBOL();
            delegado = new ThreadStart(ExtraerAeropuerto);
            hilo = new Thread(delegado);
            delegado2 = new ThreadStart(ExtraerAerolinea);
            hilo2 = new Thread(delegado2);
        }

        private void CargarAeropuerto(object sender, EventArgs e)
        {
            dgAeropuerto.DataSource = apbol.CargarDatos(Usuario);
        }

        private void CargarAerolinea(object sender, EventArgs e)
        {
            dgAerolinea.DataSource = albol.CargarDatos(Usuario);
        }

        private void ExtraerAeropuerto(object sender, EventArgs e)
        {

            if (MessageBox.Show("Esta seguro que desea actualizar los datos, puede tomar unos minutos", "Datos", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Console.WriteLine("bien uno");
                if (!activo)
                {
                    Console.WriteLine("Bien dos");
                    pbAeropuerto.Value = 0;
                    activo = true;
                    tAeropuerto.Start();
                    hilo.Start();

                }
            }
        }

        private void ExtraerAerolinea(object sender, EventArgs e)
        {

            if (MessageBox.Show("Esta seguro que desea actualizar los datos, puede tomar unos minutos", "Datos", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                if (!activo2)
                {
                    pbAerolinea.Value = 0;
                    activo2 = true;
                    tAerolinea.Start();
                    hilo2.Start();
                }
            }
        }



        private void TiempoPuerto(object sender, EventArgs e)
        {
            pbAeropuerto.Increment(1);
            if (!activo)
            {
                pbAeropuerto.PerformStep();
                tAeropuerto.Stop();
            }
        }

        private void TiempoLinea(object sender, EventArgs e)
        {
            pbAerolinea.Increment(1);
            if (!activo2)
            {
                tAerolinea.Stop();
                pbAerolinea.PerformStep();
            }
        }

        private void ExtraerAeropuerto()
        {

            apbol.Actualizar();
            activo = false;
        }

        private void ExtraerAerolinea()
        {
            albol.Actualizar();
            activo2 = false;
        }

        //hilos

        delegate void ExtraerAeropuertoDelegado();

        private void ExtraerAeropuertoCambio()
        {
            if (InvokeRequired)
            {
                ExtraerAeropuertoDelegado delegado = new ExtraerAeropuertoDelegado(ExtraerAeropuertoCambio);
                Invoke(delegado);
            }
        }
        delegate void ExtraerAerolineaDelegado();

        private void ExtraerAerolineaCambio()
        {
            if (InvokeRequired)
            {
                ExtraerAerolineaDelegado delegado = new ExtraerAerolineaDelegado(ExtraerAerolineaCambio);
                Invoke(delegado);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas eliminar este aeropuerto", "Datos", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                if (dgAeropuerto.SelectedRows.Count > 0)
                {
                    EAeropuerto aero = new EAeropuerto();
                    DataGridViewRow currentRow = dgAeropuerto.CurrentRow;
                    aero = (EAeropuerto)currentRow.DataBoundItem;
                    apbol.EliminarPorUsuario(aero, Usuario);
                    MessageBox.Show("Aeropuerto eliminado vuelva a cargar"+  aero.Fs);

                }
                else
                {
                    MessageBox.Show("Favor seleccionar un aeropuerto");
                }

            }
        }

        private void EliminarLinea_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas eliminar esta aerolinea", "Datos", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                if (dgAerolinea.SelectedRows.Count > 0)
                {
                    EAerolinea aero = new EAerolinea();
                    DataGridViewRow currentRow = dgAerolinea.CurrentRow;
                    aero = (EAerolinea)currentRow.DataBoundItem;
                    albol.EliminarPorUsuario(aero, Usuario);
                    MessageBox.Show("Aeropuerto eliminado vuelva a cargar");

                }
                else
                {
                    MessageBox.Show("Favor seleccionar un aeropuerto");
                }
            }
        }
    }
}
