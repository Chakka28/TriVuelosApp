﻿namespace Trivuelos
{
    partial class FrmBusquedaN1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnRestaurar = new System.Windows.Forms.Button();
            this.btnMaximizar = new System.Windows.Forms.Button();
            this.btnMinimizar = new System.Windows.Forms.Button();
            this.btnSalir = new System.Windows.Forms.Button();
            this.pContenedor = new System.Windows.Forms.Panel();
            this.lbNombre = new System.Windows.Forms.Label();
            this.pMenu = new System.Windows.Forms.Panel();
            this.lbInfomacion = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnDAVuelo = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnNAVuelo = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.ptbFoto = new System.Windows.Forms.PictureBox();
            this.btnSEVuelo = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.pMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ptbFoto)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(79)))), ((int)(((byte)(195)))), ((int)(((byte)(247)))));
            this.panel1.Controls.Add(this.btnRestaurar);
            this.panel1.Controls.Add(this.btnMaximizar);
            this.panel1.Controls.Add(this.btnMinimizar);
            this.panel1.Controls.Add(this.btnSalir);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1300, 29);
            this.panel1.TabIndex = 0;
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Ventana);
            // 
            // btnRestaurar
            // 
            this.btnRestaurar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRestaurar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRestaurar.FlatAppearance.BorderSize = 0;
            this.btnRestaurar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRestaurar.Location = new System.Drawing.Point(1241, 1);
            this.btnRestaurar.Name = "btnRestaurar";
            this.btnRestaurar.Size = new System.Drawing.Size(25, 25);
            this.btnRestaurar.TabIndex = 5;
            this.btnRestaurar.Text = "R";
            this.btnRestaurar.UseVisualStyleBackColor = true;
            this.btnRestaurar.Visible = false;
            this.btnRestaurar.Click += new System.EventHandler(this.btnRestaurar_Click);
            // 
            // btnMaximizar
            // 
            this.btnMaximizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMaximizar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMaximizar.FlatAppearance.BorderSize = 0;
            this.btnMaximizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMaximizar.Location = new System.Drawing.Point(1241, 1);
            this.btnMaximizar.Name = "btnMaximizar";
            this.btnMaximizar.Size = new System.Drawing.Size(25, 25);
            this.btnMaximizar.TabIndex = 4;
            this.btnMaximizar.Text = "M";
            this.btnMaximizar.UseVisualStyleBackColor = true;
            this.btnMaximizar.Click += new System.EventHandler(this.btnMaximizar_Click);
            // 
            // btnMinimizar
            // 
            this.btnMinimizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMinimizar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMinimizar.FlatAppearance.BorderSize = 0;
            this.btnMinimizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMinimizar.Location = new System.Drawing.Point(1210, 1);
            this.btnMinimizar.Name = "btnMinimizar";
            this.btnMinimizar.Size = new System.Drawing.Size(25, 25);
            this.btnMinimizar.TabIndex = 3;
            this.btnMinimizar.Text = "-";
            this.btnMinimizar.UseVisualStyleBackColor = true;
            this.btnMinimizar.Click += new System.EventHandler(this.btnMinimizar_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSalir.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSalir.FlatAppearance.BorderSize = 0;
            this.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSalir.Location = new System.Drawing.Point(1272, 1);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(25, 25);
            this.btnSalir.TabIndex = 0;
            this.btnSalir.Text = "X";
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // pContenedor
            // 
            this.pContenedor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(97)))), ((int)(((byte)(97)))), ((int)(((byte)(97)))));
            this.pContenedor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pContenedor.Location = new System.Drawing.Point(210, 29);
            this.pContenedor.Name = "pContenedor";
            this.pContenedor.Size = new System.Drawing.Size(1090, 571);
            this.pContenedor.TabIndex = 2;
            // 
            // lbNombre
            // 
            this.lbNombre.AutoSize = true;
            this.lbNombre.Location = new System.Drawing.Point(88, 31);
            this.lbNombre.Name = "lbNombre";
            this.lbNombre.Size = new System.Drawing.Size(57, 16);
            this.lbNombre.TabIndex = 2;
            this.lbNombre.Text = "Nombre";
            // 
            // pMenu
            // 
            this.pMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(64)))), ((int)(((byte)(83)))));
            this.pMenu.Controls.Add(this.lbInfomacion);
            this.pMenu.Controls.Add(this.panel4);
            this.pMenu.Controls.Add(this.btnDAVuelo);
            this.pMenu.Controls.Add(this.panel3);
            this.pMenu.Controls.Add(this.btnNAVuelo);
            this.pMenu.Controls.Add(this.panel2);
            this.pMenu.Controls.Add(this.lbNombre);
            this.pMenu.Controls.Add(this.ptbFoto);
            this.pMenu.Controls.Add(this.btnSEVuelo);
            this.pMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.pMenu.Location = new System.Drawing.Point(0, 29);
            this.pMenu.Name = "pMenu";
            this.pMenu.Size = new System.Drawing.Size(210, 571);
            this.pMenu.TabIndex = 1;
            // 
            // lbInfomacion
            // 
            this.lbInfomacion.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbInfomacion.Location = new System.Drawing.Point(12, 426);
            this.lbInfomacion.Name = "lbInfomacion";
            this.lbInfomacion.Size = new System.Drawing.Size(174, 97);
            this.lbInfomacion.TabIndex = 8;
            this.lbInfomacion.Text = "Información";
            this.lbInfomacion.Visible = false;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(79)))), ((int)(((byte)(195)))), ((int)(((byte)(247)))));
            this.panel4.Location = new System.Drawing.Point(0, 360);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(10, 30);
            this.panel4.TabIndex = 7;
            // 
            // btnDAVuelo
            // 
            this.btnDAVuelo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDAVuelo.FlatAppearance.BorderSize = 0;
            this.btnDAVuelo.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(79)))), ((int)(((byte)(195)))), ((int)(((byte)(247)))));
            this.btnDAVuelo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDAVuelo.Location = new System.Drawing.Point(9, 360);
            this.btnDAVuelo.Name = "btnDAVuelo";
            this.btnDAVuelo.Size = new System.Drawing.Size(201, 30);
            this.btnDAVuelo.TabIndex = 6;
            this.btnDAVuelo.Text = "Vuelo entre dos aeropuertos";
            this.btnDAVuelo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDAVuelo.UseVisualStyleBackColor = true;
            this.btnDAVuelo.Click += new System.EventHandler(this.AbrirDAVuelo);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(79)))), ((int)(((byte)(195)))), ((int)(((byte)(247)))));
            this.panel3.Location = new System.Drawing.Point(3, 234);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(10, 30);
            this.panel3.TabIndex = 5;
            // 
            // btnNAVuelo
            // 
            this.btnNAVuelo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNAVuelo.FlatAppearance.BorderSize = 0;
            this.btnNAVuelo.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(79)))), ((int)(((byte)(195)))), ((int)(((byte)(247)))));
            this.btnNAVuelo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNAVuelo.Location = new System.Drawing.Point(12, 234);
            this.btnNAVuelo.Name = "btnNAVuelo";
            this.btnNAVuelo.Size = new System.Drawing.Size(198, 30);
            this.btnNAVuelo.TabIndex = 4;
            this.btnNAVuelo.Text = "Numero de vuelo y aerolinea";
            this.btnNAVuelo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnNAVuelo.UseVisualStyleBackColor = true;
            this.btnNAVuelo.Click += new System.EventHandler(this.AbrirNAVuelo);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(79)))), ((int)(((byte)(195)))), ((int)(((byte)(247)))));
            this.panel2.Location = new System.Drawing.Point(3, 109);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(10, 30);
            this.panel2.TabIndex = 3;
            // 
            // ptbFoto
            // 
            this.ptbFoto.Location = new System.Drawing.Point(3, 3);
            this.ptbFoto.Name = "ptbFoto";
            this.ptbFoto.Size = new System.Drawing.Size(79, 72);
            this.ptbFoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbFoto.TabIndex = 1;
            this.ptbFoto.TabStop = false;
            // 
            // btnSEVuelo
            // 
            this.btnSEVuelo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSEVuelo.FlatAppearance.BorderSize = 0;
            this.btnSEVuelo.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(79)))), ((int)(((byte)(195)))), ((int)(((byte)(247)))));
            this.btnSEVuelo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSEVuelo.Location = new System.Drawing.Point(12, 109);
            this.btnSEVuelo.Name = "btnSEVuelo";
            this.btnSEVuelo.Size = new System.Drawing.Size(198, 30);
            this.btnSEVuelo.TabIndex = 0;
            this.btnSEVuelo.Text = "Salida y entrada de vuelos";
            this.btnSEVuelo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSEVuelo.UseVisualStyleBackColor = true;
            this.btnSEVuelo.Click += new System.EventHandler(this.AbrirSEVuelo);
            // 
            // FrmBusquedaN1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1300, 600);
            this.ControlBox = false;
            this.Controls.Add(this.pContenedor);
            this.Controls.Add(this.pMenu);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.White;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FrmBusquedaN1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmBusqueda";
            this.Load += new System.EventHandler(this.FrmBusqueda_Load);
            this.panel1.ResumeLayout(false);
            this.pMenu.ResumeLayout(false);
            this.pMenu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ptbFoto)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnMaximizar;
        private System.Windows.Forms.Button btnMinimizar;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.Button btnRestaurar;
        private System.Windows.Forms.PictureBox ptbFoto;
        private System.Windows.Forms.Label lbNombre;
        private System.Windows.Forms.Panel pMenu;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btnDAVuelo;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnNAVuelo;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnSEVuelo;
        private System.Windows.Forms.Panel pContenedor;
        private System.Windows.Forms.Label lbInfomacion;
    }
}