﻿using Facebook;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Trivuelos
{
    public partial class Facebook : Form
    {
        private readonly Uri _loginUrl;
        protected readonly FacebookClient cliente;

   

        private void Facebook_Load(object sender, EventArgs e)
        {
            webBrowser.Navigate(_loginUrl.AbsoluteUri);
            
        }
        public FacebookOAuthResult FacebookOAuthResult { get; private set; }

        public Facebook(string appId, string extendedPermissions)
            : this(new FacebookClient(), appId, extendedPermissions)
        {
            this.CenterToParent();
        }
        public Facebook(FacebookClient fb, string appId, string extendedPermissions)
        {
            if (fb == null)
                throw new ArgumentNullException("fb");
            if (string.IsNullOrWhiteSpace(appId))
                throw new ArgumentNullException("appId");


            cliente = fb;
            _loginUrl = GenerateLoginUrl(appId, extendedPermissions);
          

            InitializeComponent();
            this.CenterToParent();
        }
        private Uri GenerateLoginUrl(string appId, string extendedPermissions)
        {
            dynamic parameters = new ExpandoObject();
            parameters.client_id = appId;
            parameters.redirect_uri = "https://www.facebook.com/connect/login_success.html";


            parameters.response_type = "token";

            parameters.display = "popup";

            if (!string.IsNullOrWhiteSpace(extendedPermissions))
                parameters.scope = extendedPermissions;

            return cliente.GetLoginUrl(parameters);
        }

        
        private void webBrowser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {

        }

        private void webBrowser_Navigated_1(object sender, WebBrowserNavigatedEventArgs e)
        {
            Console.WriteLine("cdwvwdvvvwvw");
            FacebookOAuthResult oauthResult;
            if (cliente.TryParseOAuthCallbackUrl(e.Url, out oauthResult))
            {


                // The url is the result of OAuth 2.0 authentication
                FacebookOAuthResult = oauthResult;
                DialogResult = FacebookOAuthResult.IsSuccess ? DialogResult.OK : DialogResult.No;
            }
            else
            {
                Console.WriteLine("fffffffffffffffffffffffffff");
                // The url is NOT the result of OAuth 2.0 authentication.
                FacebookOAuthResult = null;
            }
        }
    }
}
