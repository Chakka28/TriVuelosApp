﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Trivuelosentities;
using TrivuelosBOL;


namespace Trivuelos
{
    public partial class FrmNAVuelo : Form
    {
        private VueloProgramadoBOL vpbol;
        private AerolineaBOL albol;
        public EUsuario Logeado;

        public FrmNAVuelo()
        {
            InitializeComponent();
        }

        private void FrmNAVuelo_Load(object sender, EventArgs e)
        {
            vpbol = new VueloProgramadoBOL();
            albol = new AerolineaBOL();
            cbxAerolinea.DataSource = albol.CargarDatos(Logeado) ?? null;
        }

        private void BuscarSalida(object sender, EventArgs e)
        {
            try
            {
                lbInformacion.Text = "Cargando los vuelos, espere por favor...";
                lbInformacion.Visible = true;
                dgVuelos.DataSource = vpbol.ObtenerVuelosAerolineaSalida(Configuracion());
                lbInformacion.Visible = false;
            }
            catch (Exception ex)
            {

                lbInformacion.Text = ex.Message;
            }
        }

        private void BuscarLlegada(object sender, EventArgs e)
        {
            try
            {
                lbInformacion.Text = "Cargando los vuelos, espere por favor...";
                lbInformacion.Visible = true;
                dgVuelos.DataSource = vpbol.ObtenerVuelosAerolineaLlegada(Configuracion());
                lbInformacion.Visible = false;
            }
            catch (Exception ex)
            {

                lbInformacion.Text = ex.Message;
            }
        }

        /// <summary>
        /// Configuración basada en los datos datos por el usuario
        /// </summary>
        /// <returns>Configuración del usuario</returns>
        private EConfiguracionVuelo Configuracion()
        {
            EConfiguracionVuelo con = new EConfiguracionVuelo
            {
                Aerolinea = (cbxAerolinea.SelectedItem as EAerolinea).Fs,
                Anno = dtpFechaOperador.Value.Year,
                Mes = dtpFechaOperador.Value.Month,
                Dia = dtpFechaOperador.Value.Day,
                NumeroVuelo = txtNumeroVuelo.Text.Trim()

            };
            return con;
        }
    }
}
