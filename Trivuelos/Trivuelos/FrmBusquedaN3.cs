﻿using GMap.NET;
using GMap.NET.MapProviders;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrivuelosBOL;
using Trivuelosentities;
using Trivuelos.WSVueloSeguimiento;
using GMap.NET.WindowsForms;
using GMap.NET.WindowsForms.Markers;
using System.Threading;

namespace Trivuelos
{
    public partial class FrmBusquedaN3 : Form
    {
        private SeguimientoVueloBOL sbol;
        private EPunto[] puntos;
        private EPosicion[] pos;
        private GMapOverlay zona;
        private GMapOverlay zonaVuelo;
        public EConfiguracionVuelo configuracion;
        public EVuelo vuelo;
        public int bandera;

        public FrmBusquedaN3()
        {
            InitializeComponent();
        }

        private void gmVuelo_Load(object sender, EventArgs e)
        {

        }

        private void FrmBusquedaN3_Load(object sender, EventArgs e)
        {
            sbol = new SeguimientoVueloBOL();
            zona = new GMapOverlay();
            zonaVuelo = new GMapOverlay();
            if (bandera == 1)
            {
                CargarMapaSinId();
                RutaAvion(sender, e);
            }

        }

        private void btnRuta_Click(object sender, EventArgs e)
        {
            try
            {
                gmVuelo.Overlays.Clear();
                zona.Clear();
                long id = sbol.Valido(txtIDvuelo.Text.Trim());
                CargarMapa(id);
                RutaAvion(sender, e);
            }
            catch (Exception ex)
            {
                lbVuelo.Visible = true;
                lbVuelo.Text = ex.Message;
            }
        }

        /// <summary>
        /// Carga los datos a mostrar en el mapa
        /// </summary>
        /// <param name="id"></param>
        private void CargarMapa(long id)
        {
            try
            {
                lbVuelo.Text = "Cargando ruta...";
                lbVuelo.Visible = true;
                Thread.Sleep(300);
                configuracion = sbol.CargarConfiguracion(id);
                puntos = sbol.CargarPuntos(id);
                pos = sbol.CargarPosiciones(id);
                gmVuelo.DragButton = MouseButtons.Left;
                gmVuelo.MapProvider = GMapProviders.GoogleMap;
                gmVuelo.Position = new PointLatLng(puntos[0].Lat, puntos[0].Lon);
                gmVuelo.Zoom = 2;

                gmVuelo.Overlays.Add(CargarRuta("Lugar", puntos));
                AgregarMarcador(Marcador(puntos[0].Lat, puntos[0].Lon, ("Salida: " + configuracion.Aeropuerto), GMarkerGoogleType.green));
                AgregarMarcador(Marcador(puntos[puntos.Length - 1].Lat, puntos[puntos.Length - 1].Lon, ("Destino: " + configuracion.Aeropuerto2), GMarkerGoogleType.green));

                gmVuelo.Overlays.Add(zona);

                gmVuelo.Zoom = gmVuelo.Zoom + 1;
                gmVuelo.Zoom = gmVuelo.Zoom - 1;
                lbVuelo.Visible = false;
            }
            catch (Exception ex)
            {

                lbVuelo.Text = ex.Message;
            }
        }

        /// <summary>
        /// Cargar el mapa sin digitar un id del vuelo
        /// </summary>
        private void CargarMapaSinId()
        {
            try
            {
                lbVuelo.Text = "cargando vuelo";
                lbVuelo.Visible = true;
                Thread.Sleep(300);
                puntos = sbol.CargarPuntosFecha(configuracion);
                gmVuelo.DragButton = MouseButtons.Left;
                gmVuelo.MapProvider = GMapProviders.GoogleMap;
                gmVuelo.Position = new PointLatLng(puntos[0].Lat, puntos[0].Lon);
                gmVuelo.Zoom = 2;

                gmVuelo.Overlays.Add(CargarRuta("Lugar", sbol.CargarPuntosFecha(configuracion)));
                AgregarMarcador(Marcador(puntos[0].Lat, puntos[0].Lon, ("E"+configuracion.Aeropuerto), GMarkerGoogleType.green));
                AgregarMarcador(Marcador(puntos[puntos.Length - 1].Lat, puntos[puntos.Length - 1].Lon, configuracion.Aeropuerto2, GMarkerGoogleType.green));
                gmVuelo.Overlays.Add(zona);

                gmVuelo.Zoom = gmVuelo.Zoom + 1;
                gmVuelo.Zoom = gmVuelo.Zoom - 1;
                lbVuelo.Visible = false;
            }
            catch (Exception ex)
            {

                lbVuelo.Text = ex.Message;
            }
        }

        /// <summary>
        /// Crea un nuevo marcador
        /// </summary>
        /// <param name="lat"></param>
        /// <param name="lon"></param>
        /// <returns></returns>
        private GMarkerGoogle Marcador(double lat, double lon, string valor, GMarkerGoogleType color)
        {
            GMarkerGoogle marcador = new GMarkerGoogle(new PointLatLng(lat, lon), color);
            marcador.ToolTipMode = MarkerTooltipMode.Always;
            marcador.ToolTipText = String.Format("{0} \n latitud: {1} \n longitud{2}", valor, lat, lon);

            return marcador;
        }

        /// <summary>
        /// Agrega un nuevop marcador al mapa
        /// </summary>
        /// <param name="marcador"></param>
        private void AgregarMarcador(GMarkerGoogle marcador)
        {
            zona.Markers.Add(marcador);
        }

        /// <summary>
        /// Carga la ruta delvuelo
        /// </summary>
        /// <param name="nombre"></param>
        /// <param name="puntos"></param>
        /// <returns>GMapOverlay con la ruta del vuelo</returns>
        private GMapOverlay CargarRuta(string nombre, EPunto[] puntos)
        {
            GMapOverlay Ruta = new GMapOverlay(nombre);
            List<PointLatLng> listaPuntos = new List<PointLatLng>();

            foreach (EPunto p in puntos)
            {
                listaPuntos.Add(new PointLatLng(p.Lat, p.Lon));
            }

            GMapRoute PuntosRuta = new GMapRoute(listaPuntos, nombre);
            Ruta.Routes.Add(PuntosRuta);
            return Ruta;
        }

        /// <summary>
        /// Cargar la ruta del avión
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RutaAvion(object sender, EventArgs e)
        {
            try
            {
                if (bandera == 1)
                {
                    pos = sbol.CargarPosicionesFecha(configuracion);
                    if (pos != null)
                    {
                        zonaVuelo.Markers.Clear();
                        zonaVuelo.Markers.Add(Marcador(pos[0].Latitud, pos[0].Logitud, configuracion.Aerolinea, GMarkerGoogleType.red));
                        gmVuelo.Overlays.Add(zonaVuelo);
                        gmVuelo.Zoom = gmVuelo.Zoom + 1;
                        gmVuelo.Zoom = gmVuelo.Zoom - 1;
                    }
                }
                else
                {
                    zonaVuelo.Markers.Clear();
                    zonaVuelo.Markers.Add(Marcador(pos[0].Latitud, pos[0].Logitud, configuracion.Aerolinea, GMarkerGoogleType.red));
                    gmVuelo.Overlays.Add(zonaVuelo);
                    gmVuelo.Zoom = gmVuelo.Zoom + 1;
                    gmVuelo.Zoom = gmVuelo.Zoom - 1;
                }
            }
            catch (Exception ex)
            {

                lbVuelo.Text = ex.Message;

            }

        }

        private void Volver(object sender, EventArgs e)
        {
            Close();
        }
    }
}
