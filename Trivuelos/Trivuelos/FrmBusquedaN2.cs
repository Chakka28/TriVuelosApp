﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Trivuelos.WSClima;
using Trivuelos.WSVuelos;
using TrivuelosBOL;
using Trivuelosentities;

namespace Trivuelos
{
    public partial class FrmBusquedaN2 : Form
    {

        private AerolineaBOL alBOL;
        internal EUsuario Usuario { get; set; }


        public FrmBusquedaN2()
        {

            InitializeComponent();
        }

        private void FrmBusquedaN2_Load(object sender, EventArgs e)
        {
            alBOL = new AerolineaBOL();
            cbal.DataSource = alBOL.CargarDatos(Usuario);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            cargar();
        }


        /// <summary>
        /// Carga datos de la api 
        /// </summary>
        private void cargar()
        {
            if (txtNumVuelo.Text != "")
            {
                ///Conexion
                flightServiceClient ws = new flightServiceClient("flightServicePort");
                responseFlightStatus xmlText = ws.flightStatus_arr("ed9a785b", "d8870be5ac62b2d6ddcfb041ea4aaf3d",
                   (string)cbal.SelectedValue, txtNumVuelo.Text,
                   FechaVuelo.Value.Year,
                   FechaVuelo.Value.Month,
                   FechaVuelo.Value.Day,
                   "false", "", "", "");


                ///lista de estado
                flightStatusV2[] estatus = xmlText.flightStatuses;


                if (estatus.Length > 0)
                {
                    ///Lista de aeropuertos
                    WSVuelos.airportV1[] aeropuertos = xmlText.appendix.airports;
                    lblAeroLLegada.Text = aeropuertos[0].name;
                    lblAeroSalida.Text = aeropuertos[1].name;

                    ///Puertas
                    lblPuertaEntrada.Text = estatus[0].airportResources.departureGate;
                    lblPuertaSalida.Text = estatus[0].airportResources.arrivalGate;


                    ///Tipo de vuelo
                    lblTipo.Text = estatus[0].schedule.flightType;


                    ///Estado
                    if (!estatus[0].status.Equals("C"))
                    {
                        DateTime scheduledfecha = Convert.ToDateTime(estatus[0].operationalTimes.scheduledGateArrival.dateLocal);
                        DateTime fechaActual = DateTime.Now;

                        int result1 = DateTime.Compare(FechaVuelo.Value, fechaActual);
                        if (result1 < 0)
                        {
                            try
                            {
                                DateTime actualfecha = Convert.ToDateTime(estatus[0].operationalTimes.actualGateArrival.dateLocal);
                                int result = DateTime.Compare(scheduledfecha, actualfecha);
                                if (result < 0)
                                    lblEstado.Text = "Retraso";
                                else
                                    lblEstado.Text = "A tiempo";

                            }
                            catch (Exception)
                            {
                                Cargar_Estado(scheduledfecha, estatus);
                            }


                        }
                        else
                        {
                            Cargar_Estado(scheduledfecha, estatus);
                        }

                        CargarClima(xmlText.appendix.airports[0].fs);
                    }
                    else
                    {
                        lblEstado.Text = "CANCELADO";
                    }
                }
                else
                {
                    MessageBox.Show("Vuelo no existe");
                }
            }
            else
            {
                MessageBox.Show("Escriba un vuelo");
            }


        }

        /// <summary>
        /// Este metodo carga los estado en el caso de que la fecha elegida sea mayor 
        /// </summary>
        /// <param name="scheduledfecha">fecha porgramada del vuelo</param>
        /// <param name="estatus">lista de los estado</param>
        private void Cargar_Estado(DateTime scheduledfecha, flightStatusV2[] estatus)
        {
            try
            {
                DateTime actualfecha = Convert.ToDateTime(estatus[0].operationalTimes.estimatedGateArrival.dateLocal);

                int result = DateTime.Compare(scheduledfecha, actualfecha);
                if (result < 0)
                    lblEstado.Text = "Retraso";
                else
                    lblEstado.Text = "A tiempo";
            }
            catch (Exception)
            {
                lblEstado.Text = "A tiempo";
            }

        }

        /// <summary>
        /// Carga el clima
        /// </summary>
        /// <param name="aeropuerto">aeropuerto del clima</param>
        private void CargarClima(string aeropuerto)
        {
            weatherServiceClient ws = new weatherServiceClient();
            allWeatherResponseV1 clima = ws.all("ed9a785b", "d8870be5ac62b2d6ddcfb041ea4aaf3d", aeropuerto, "", "");
            lblClima.Text = clima.metar.conditions.skyConditions[0].coverage;
            lblTemperatura.Text = clima.metar.temperatureCelsius + " C";

        }
    }
}
