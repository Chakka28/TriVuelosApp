﻿namespace Trivuelos
{
    partial class FrmNAVuelo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label3 = new System.Windows.Forms.Label();
            this.btnLlegada = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSalida = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.dgVuelos = new System.Windows.Forms.DataGridView();
            this.llegadaAeropuertoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.horaSalidaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.horaLlegadaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.salidaAeropuertoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.portadorDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numeroVueloDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vueloEquipoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.paradaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.eVueloBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cbxAerolinea = new System.Windows.Forms.ComboBox();
            this.eAerolineaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.txtNumeroVuelo = new System.Windows.Forms.TextBox();
            this.dtpFechaOperador = new System.Windows.Forms.DateTimePicker();
            this.lbInformacion = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgVuelos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eVueloBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eAerolineaBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(289, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(312, 18);
            this.label3.TabIndex = 32;
            this.label3.Text = "Vuelo por aerolínea y número de vuelo";
            // 
            // btnLlegada
            // 
            this.btnLlegada.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLlegada.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(73)))), ((int)(((byte)(94)))));
            this.btnLlegada.FlatAppearance.BorderSize = 0;
            this.btnLlegada.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLlegada.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLlegada.ForeColor = System.Drawing.Color.White;
            this.btnLlegada.Location = new System.Drawing.Point(584, 149);
            this.btnLlegada.Name = "btnLlegada";
            this.btnLlegada.Size = new System.Drawing.Size(170, 27);
            this.btnLlegada.TabIndex = 31;
            this.btnLlegada.Text = "Buscar por llegada";
            this.btnLlegada.UseVisualStyleBackColor = false;
            this.btnLlegada.Click += new System.EventHandler(this.BuscarLlegada);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(623, 86);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 17);
            this.label2.TabIndex = 30;
            this.label2.Text = "Fecha";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(310, 86);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(131, 17);
            this.label1.TabIndex = 29;
            this.label1.Text = "Número de vuelo";
            // 
            // btnSalida
            // 
            this.btnSalida.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(73)))), ((int)(((byte)(94)))));
            this.btnSalida.FlatAppearance.BorderSize = 0;
            this.btnSalida.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSalida.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalida.ForeColor = System.Drawing.Color.White;
            this.btnSalida.Location = new System.Drawing.Point(57, 149);
            this.btnSalida.Name = "btnSalida";
            this.btnSalida.Size = new System.Drawing.Size(170, 27);
            this.btnSalida.TabIndex = 28;
            this.btnSalida.Text = "Buscar por salida";
            this.btnSalida.UseVisualStyleBackColor = false;
            this.btnSalida.Click += new System.EventHandler(this.BuscarSalida);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(68, 86);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(79, 17);
            this.label8.TabIndex = 27;
            this.label8.Text = "Aerolínea";
            // 
            // dgVuelos
            // 
            this.dgVuelos.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.dgVuelos.AutoGenerateColumns = false;
            this.dgVuelos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgVuelos.BackgroundColor = System.Drawing.Color.DimGray;
            this.dgVuelos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgVuelos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.llegadaAeropuertoDataGridViewTextBoxColumn,
            this.horaSalidaDataGridViewTextBoxColumn,
            this.horaLlegadaDataGridViewTextBoxColumn,
            this.salidaAeropuertoDataGridViewTextBoxColumn,
            this.portadorDataGridViewTextBoxColumn,
            this.numeroVueloDataGridViewTextBoxColumn,
            this.vueloEquipoDataGridViewTextBoxColumn,
            this.paradaDataGridViewTextBoxColumn});
            this.dgVuelos.DataSource = this.eVueloBindingSource;
            this.dgVuelos.Location = new System.Drawing.Point(8, 182);
            this.dgVuelos.Name = "dgVuelos";
            this.dgVuelos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullColumnSelect;
            this.dgVuelos.Size = new System.Drawing.Size(794, 234);
            this.dgVuelos.TabIndex = 26;
            // 
            // llegadaAeropuertoDataGridViewTextBoxColumn
            // 
            this.llegadaAeropuertoDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.llegadaAeropuertoDataGridViewTextBoxColumn.DataPropertyName = "LlegadaAeropuerto";
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            this.llegadaAeropuertoDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle1;
            this.llegadaAeropuertoDataGridViewTextBoxColumn.HeaderText = "Destino aeropuerto";
            this.llegadaAeropuertoDataGridViewTextBoxColumn.Name = "llegadaAeropuertoDataGridViewTextBoxColumn";
            this.llegadaAeropuertoDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // horaSalidaDataGridViewTextBoxColumn
            // 
            this.horaSalidaDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.horaSalidaDataGridViewTextBoxColumn.DataPropertyName = "HoraSalida";
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            this.horaSalidaDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.horaSalidaDataGridViewTextBoxColumn.HeaderText = "Hora de salida";
            this.horaSalidaDataGridViewTextBoxColumn.Name = "horaSalidaDataGridViewTextBoxColumn";
            this.horaSalidaDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // horaLlegadaDataGridViewTextBoxColumn
            // 
            this.horaLlegadaDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.horaLlegadaDataGridViewTextBoxColumn.DataPropertyName = "HoraLlegada";
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.White;
            this.horaLlegadaDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this.horaLlegadaDataGridViewTextBoxColumn.HeaderText = "Hora de llegada";
            this.horaLlegadaDataGridViewTextBoxColumn.Name = "horaLlegadaDataGridViewTextBoxColumn";
            this.horaLlegadaDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // salidaAeropuertoDataGridViewTextBoxColumn
            // 
            this.salidaAeropuertoDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.salidaAeropuertoDataGridViewTextBoxColumn.DataPropertyName = "SalidaAeropuerto";
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.White;
            this.salidaAeropuertoDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle4;
            this.salidaAeropuertoDataGridViewTextBoxColumn.HeaderText = "Salida del aeropuerto";
            this.salidaAeropuertoDataGridViewTextBoxColumn.Name = "salidaAeropuertoDataGridViewTextBoxColumn";
            this.salidaAeropuertoDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // portadorDataGridViewTextBoxColumn
            // 
            this.portadorDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.portadorDataGridViewTextBoxColumn.DataPropertyName = "Portador";
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.White;
            this.portadorDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle5;
            this.portadorDataGridViewTextBoxColumn.HeaderText = "Aerolínea";
            this.portadorDataGridViewTextBoxColumn.Name = "portadorDataGridViewTextBoxColumn";
            this.portadorDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // numeroVueloDataGridViewTextBoxColumn
            // 
            this.numeroVueloDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.numeroVueloDataGridViewTextBoxColumn.DataPropertyName = "NumeroVuelo";
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.White;
            this.numeroVueloDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle6;
            this.numeroVueloDataGridViewTextBoxColumn.HeaderText = "NumeroVuelo";
            this.numeroVueloDataGridViewTextBoxColumn.Name = "numeroVueloDataGridViewTextBoxColumn";
            this.numeroVueloDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // vueloEquipoDataGridViewTextBoxColumn
            // 
            this.vueloEquipoDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.vueloEquipoDataGridViewTextBoxColumn.DataPropertyName = "VueloEquipo";
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.White;
            this.vueloEquipoDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle7;
            this.vueloEquipoDataGridViewTextBoxColumn.HeaderText = "VueloEquipo";
            this.vueloEquipoDataGridViewTextBoxColumn.Name = "vueloEquipoDataGridViewTextBoxColumn";
            this.vueloEquipoDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // paradaDataGridViewTextBoxColumn
            // 
            this.paradaDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.paradaDataGridViewTextBoxColumn.DataPropertyName = "Parada";
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.White;
            this.paradaDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle8;
            this.paradaDataGridViewTextBoxColumn.HeaderText = "Parada";
            this.paradaDataGridViewTextBoxColumn.Name = "paradaDataGridViewTextBoxColumn";
            this.paradaDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // eVueloBindingSource
            // 
            this.eVueloBindingSource.DataSource = typeof(Trivuelosentities.EVuelo);
            // 
            // cbxAerolinea
            // 
            this.cbxAerolinea.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.eAerolineaBindingSource, "Fs", true));
            this.cbxAerolinea.DataSource = this.eAerolineaBindingSource;
            this.cbxAerolinea.DisplayMember = "Nombre";
            this.cbxAerolinea.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxAerolinea.FormattingEnabled = true;
            this.cbxAerolinea.Location = new System.Drawing.Point(21, 110);
            this.cbxAerolinea.Name = "cbxAerolinea";
            this.cbxAerolinea.Size = new System.Drawing.Size(180, 25);
            this.cbxAerolinea.TabIndex = 25;
            this.cbxAerolinea.ValueMember = "Fs";
            // 
            // eAerolineaBindingSource
            // 
            this.eAerolineaBindingSource.DataSource = typeof(Trivuelosentities.EAerolinea);
            // 
            // txtNumeroVuelo
            // 
            this.txtNumeroVuelo.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtNumeroVuelo.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNumeroVuelo.Location = new System.Drawing.Point(301, 110);
            this.txtNumeroVuelo.Name = "txtNumeroVuelo";
            this.txtNumeroVuelo.Size = new System.Drawing.Size(150, 25);
            this.txtNumeroVuelo.TabIndex = 24;
            // 
            // dtpFechaOperador
            // 
            this.dtpFechaOperador.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dtpFechaOperador.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpFechaOperador.Location = new System.Drawing.Point(504, 110);
            this.dtpFechaOperador.Name = "dtpFechaOperador";
            this.dtpFechaOperador.Size = new System.Drawing.Size(292, 25);
            this.dtpFechaOperador.TabIndex = 23;
            // 
            // lbInformacion
            // 
            this.lbInformacion.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbInformacion.Location = new System.Drawing.Point(5, 9);
            this.lbInformacion.Name = "lbInformacion";
            this.lbInformacion.Size = new System.Drawing.Size(271, 67);
            this.lbInformacion.TabIndex = 33;
            this.lbInformacion.Text = "Información";
            this.lbInformacion.Visible = false;
            // 
            // FrmNAVuelo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(97)))), ((int)(((byte)(97)))), ((int)(((byte)(97)))));
            this.ClientSize = new System.Drawing.Size(816, 424);
            this.Controls.Add(this.lbInformacion);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnLlegada);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnSalida);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.dgVuelos);
            this.Controls.Add(this.cbxAerolinea);
            this.Controls.Add(this.txtNumeroVuelo);
            this.Controls.Add(this.dtpFechaOperador);
            this.ForeColor = System.Drawing.Color.White;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmNAVuelo";
            this.Text = "FrmNAVuelo";
            this.Load += new System.EventHandler(this.FrmNAVuelo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgVuelos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eVueloBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eAerolineaBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnLlegada;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSalida;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DataGridView dgVuelos;
        private System.Windows.Forms.ComboBox cbxAerolinea;
        private System.Windows.Forms.TextBox txtNumeroVuelo;
        private System.Windows.Forms.DateTimePicker dtpFechaOperador;
        private System.Windows.Forms.BindingSource eAerolineaBindingSource;
        private System.Windows.Forms.BindingSource eVueloBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn llegadaAeropuertoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn horaSalidaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn horaLlegadaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn salidaAeropuertoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn portadorDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn numeroVueloDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn vueloEquipoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn paradaDataGridViewTextBoxColumn;
        private System.Windows.Forms.Label lbInformacion;
    }
}