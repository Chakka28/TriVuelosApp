﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Trivuelosentities;

namespace Trivuelos
{
    public partial class FrmPrincipal : Form
    {
        public EUsuario Usuario { get; set; }

        public FrmPrincipal()
        {
            InitializeComponent();
        }

        private void FrmPrincipal_Load(object sender, EventArgs e)
        {
            pbfoto.Image = Usuario.Foto;
            lblNombre.Text = Usuario.Nombre;


        }

        private void label1_Click(object sender, EventArgs e)
        {

        }


        /// <summary>
        /// LLama a la interface de de mantenimiento
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCargarAeropuertos_Click(object sender, EventArgs e)
        {
            Hide();
            new Prueba() { Usuario = Usuario }.ShowDialog();
            Show();
        }

        /// <summary>
        /// Llama a la interface de retrasos
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRetrasos_Click(object sender, EventArgs e)
        {
            Hide();
            new FrmBusquedaN4() { }.ShowDialog();
            Show();
        }


        /// <summary>
        /// Llama a la interface de estado
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEstadoVuelo_Click(object sender, EventArgs e)
        {
            Hide();
            new FrmBusquedaN2() { Usuario = Usuario }.ShowDialog();
            Show();
        }

        /// <summary>
        /// Llama a la interface de busqueda vuelos
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BusquedaVuelo(object sender, EventArgs e)
        {
            Hide();
            new FrmBusquedaN1() { Logeado = Usuario }.ShowDialog();
            Show();
        }

        /// <summary>
        /// llama a la interface de seguimientos vuelo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SeguimientoVuelo(object sender, EventArgs e)
        {
            Hide();
            new FrmBusquedaN3() { bandera = 0 }.ShowDialog();
            Show();
        }
    }
}
