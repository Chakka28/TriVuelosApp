﻿namespace Trivuelos
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnGoogle = new System.Windows.Forms.Button();
            this.btnFacebook = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnGoogle
            // 
            this.btnGoogle.Image = global::Trivuelos.Properties.Resources.google;
            this.btnGoogle.Location = new System.Drawing.Point(96, 190);
            this.btnGoogle.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnGoogle.Name = "btnGoogle";
            this.btnGoogle.Size = new System.Drawing.Size(309, 46);
            this.btnGoogle.TabIndex = 0;
            this.btnGoogle.UseVisualStyleBackColor = true;
            this.btnGoogle.Click += new System.EventHandler(this.IniciarSesionGoogle);
            // 
            // btnFacebook
            // 
            this.btnFacebook.Image = global::Trivuelos.Properties.Resources.facebook;
            this.btnFacebook.Location = new System.Drawing.Point(96, 245);
            this.btnFacebook.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnFacebook.Name = "btnFacebook";
            this.btnFacebook.Size = new System.Drawing.Size(309, 46);
            this.btnFacebook.TabIndex = 1;
            this.btnFacebook.UseVisualStyleBackColor = true;
            this.btnFacebook.Click += new System.EventHandler(this.button2_Click);
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Trivuelos.Properties.Resources.PORTADA;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(501, 303);
            this.Controls.Add(this.btnFacebook);
            this.Controls.Add(this.btnGoogle);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Login";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "trivuelos";
            this.Load += new System.EventHandler(this.Login_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnGoogle;
        private System.Windows.Forms.Button btnFacebook;
    }
}