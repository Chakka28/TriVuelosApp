﻿namespace Trivuelos
{
    partial class FrmBusquedaN3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gmVuelo = new GMap.NET.WindowsForms.GMapControl();
            this.txtIDvuelo = new System.Windows.Forms.TextBox();
            this.btnRuta = new System.Windows.Forms.Button();
            this.tRuta = new System.Windows.Forms.Timer(this.components);
            this.lbVuelo = new System.Windows.Forms.Label();
            this.btnVolver = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // gmVuelo
            // 
            this.gmVuelo.Bearing = 0F;
            this.gmVuelo.CanDragMap = true;
            this.gmVuelo.EmptyTileColor = System.Drawing.Color.Navy;
            this.gmVuelo.GrayScaleMode = false;
            this.gmVuelo.HelperLineOption = GMap.NET.WindowsForms.HelperLineOptions.DontShow;
            this.gmVuelo.LevelsKeepInMemmory = 5;
            this.gmVuelo.Location = new System.Drawing.Point(12, 40);
            this.gmVuelo.MarkersEnabled = true;
            this.gmVuelo.MaxZoom = 24;
            this.gmVuelo.MinZoom = 0;
            this.gmVuelo.MouseWheelZoomEnabled = true;
            this.gmVuelo.MouseWheelZoomType = GMap.NET.MouseWheelZoomType.MousePositionAndCenter;
            this.gmVuelo.Name = "gmVuelo";
            this.gmVuelo.NegativeMode = false;
            this.gmVuelo.PolygonsEnabled = true;
            this.gmVuelo.RetryLoadTile = 0;
            this.gmVuelo.RoutesEnabled = true;
            this.gmVuelo.ScaleMode = GMap.NET.WindowsForms.ScaleModes.Integer;
            this.gmVuelo.SelectedAreaFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(65)))), ((int)(((byte)(105)))), ((int)(((byte)(225)))));
            this.gmVuelo.ShowTileGridLines = false;
            this.gmVuelo.Size = new System.Drawing.Size(855, 522);
            this.gmVuelo.TabIndex = 0;
            this.gmVuelo.Zoom = 9D;
            this.gmVuelo.Load += new System.EventHandler(this.gmVuelo_Load);
            // 
            // txtIDvuelo
            // 
            this.txtIDvuelo.Location = new System.Drawing.Point(93, 13);
            this.txtIDvuelo.Name = "txtIDvuelo";
            this.txtIDvuelo.Size = new System.Drawing.Size(118, 20);
            this.txtIDvuelo.TabIndex = 1;
            // 
            // btnRuta
            // 
            this.btnRuta.FlatAppearance.BorderSize = 0;
            this.btnRuta.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRuta.ForeColor = System.Drawing.Color.White;
            this.btnRuta.Location = new System.Drawing.Point(12, 11);
            this.btnRuta.Name = "btnRuta";
            this.btnRuta.Size = new System.Drawing.Size(75, 23);
            this.btnRuta.TabIndex = 2;
            this.btnRuta.Text = "Nueva ruta";
            this.btnRuta.UseVisualStyleBackColor = true;
            this.btnRuta.Click += new System.EventHandler(this.btnRuta_Click);
            // 
            // tRuta
            // 
            this.tRuta.Interval = 1000;
            this.tRuta.Tick += new System.EventHandler(this.RutaAvion);
            // 
            // lbVuelo
            // 
            this.lbVuelo.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVuelo.ForeColor = System.Drawing.Color.White;
            this.lbVuelo.Location = new System.Drawing.Point(229, 9);
            this.lbVuelo.Name = "lbVuelo";
            this.lbVuelo.Size = new System.Drawing.Size(576, 24);
            this.lbVuelo.TabIndex = 3;
            this.lbVuelo.Text = "vuelo";
            this.lbVuelo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbVuelo.Visible = false;
            // 
            // btnVolver
            // 
            this.btnVolver.FlatAppearance.BorderSize = 0;
            this.btnVolver.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVolver.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVolver.ForeColor = System.Drawing.Color.White;
            this.btnVolver.Location = new System.Drawing.Point(851, -2);
            this.btnVolver.Name = "btnVolver";
            this.btnVolver.Size = new System.Drawing.Size(27, 24);
            this.btnVolver.TabIndex = 4;
            this.btnVolver.Text = "X";
            this.btnVolver.UseVisualStyleBackColor = true;
            this.btnVolver.Click += new System.EventHandler(this.Volver);
            // 
            // FrmBusquedaN3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(64)))), ((int)(((byte)(83)))));
            this.ClientSize = new System.Drawing.Size(879, 574);
            this.Controls.Add(this.btnVolver);
            this.Controls.Add(this.lbVuelo);
            this.Controls.Add(this.btnRuta);
            this.Controls.Add(this.txtIDvuelo);
            this.Controls.Add(this.gmVuelo);
            this.Name = "FrmBusquedaN3";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmBusqueda3";
            this.Load += new System.EventHandler(this.FrmBusquedaN3_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private GMap.NET.WindowsForms.GMapControl gmVuelo;
        private System.Windows.Forms.TextBox txtIDvuelo;
        private System.Windows.Forms.Button btnRuta;
        private System.Windows.Forms.Timer tRuta;
        private System.Windows.Forms.Label lbVuelo;
        private System.Windows.Forms.Button btnVolver;
    }
}