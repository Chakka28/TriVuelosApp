﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Trivuelosentities;
using TrivuelosBOL;

namespace Trivuelos
{
    public partial class FrmDAVuelo : Form
    {
        private VueloProgramadoBOL vpbol;
        private AeropuertoBOL apbol;
        public EUsuario Logeado;

        public FrmDAVuelo()
        {
            InitializeComponent();
        }

        private void FrmDAVuelo_Load(object sender, EventArgs e)
        {
            vpbol = new VueloProgramadoBOL();
            apbol = new AeropuertoBOL();
            cbxAero1.DataSource = apbol.CargarDatos(Logeado) ?? null;
            cbxAero2.DataSource = cbxAero1.DataSource;
        }

        private void BuscarVuelo(object sender, EventArgs e)
        {
            try
            {
                lbInformacion.Text = "Cargando los vuelos, espere por favor...";
                lbInformacion.Visible = true;
                dgVuelos.DataSource = vpbol.ObtenerVuelosEntreAeropuerto(Configuracion());
                lbInformacion.Visible = false;
               
            }
            catch (Exception ex)
            {
                lbInformacion.Text = ex.Message;
            }

        }

        /// <summary>
        /// onfiguración basada en los datos datos por el usuario
        /// </summary>
        /// <returns>Configuración del usuario</returns>
        private EConfiguracionVuelo Configuracion()
        {
            EConfiguracionVuelo con = new EConfiguracionVuelo
            {
                Aeropuerto = (cbxAero1.SelectedItem as EAeropuerto).Fs,
                Aeropuerto2 = (cbxAero2.SelectedItem as EAeropuerto).Fs,
                Anno = dtmDosAero.Value.Year,
                Mes = dtmDosAero.Value.Month,
                Dia = dtmDosAero.Value.Day
            };
            return con;
        }
    }
}
