﻿namespace Trivuelos
{
    partial class Prueba
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnAeropuertoAPI = new System.Windows.Forms.Button();
            this.btnAerolineaAPI = new System.Windows.Forms.Button();
            this.dgAeropuerto = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.faaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iataDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.icaoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nombreDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cuidadDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codigoEstadoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codigoPaisDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nombrePaisDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nombreRegionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tiempoZonaRegionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.latitudDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.longitudDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.elevacionPieDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.activoDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.clasificacionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.eAeropuertoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dgAerolinea = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fsDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iataDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.icaoDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nombreDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.activoDataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.eAerolineaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.btnAeropuertoDB = new System.Windows.Forms.Button();
            this.btnAerolineaBD = new System.Windows.Forms.Button();
            this.pbAerolinea = new System.Windows.Forms.ProgressBar();
            this.pbAeropuerto = new System.Windows.Forms.ProgressBar();
            this.tAeropuerto = new System.Windows.Forms.Timer(this.components);
            this.tAerolinea = new System.Windows.Forms.Timer(this.components);
            this.EliminarAero = new System.Windows.Forms.Button();
            this.EliminarLinea = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgAeropuerto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eAeropuertoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgAerolinea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eAerolineaBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnAeropuertoAPI
            // 
            this.btnAeropuertoAPI.Location = new System.Drawing.Point(45, 50);
            this.btnAeropuertoAPI.Name = "btnAeropuertoAPI";
            this.btnAeropuertoAPI.Size = new System.Drawing.Size(172, 39);
            this.btnAeropuertoAPI.TabIndex = 0;
            this.btnAeropuertoAPI.Text = "Extraer Aeropuertos a Trivuelos";
            this.btnAeropuertoAPI.UseVisualStyleBackColor = true;
            this.btnAeropuertoAPI.Click += new System.EventHandler(this.ExtraerAeropuerto);
            // 
            // btnAerolineaAPI
            // 
            this.btnAerolineaAPI.Location = new System.Drawing.Point(45, 382);
            this.btnAerolineaAPI.Name = "btnAerolineaAPI";
            this.btnAerolineaAPI.Size = new System.Drawing.Size(172, 39);
            this.btnAerolineaAPI.TabIndex = 1;
            this.btnAerolineaAPI.Text = "Extraer Aerolineas a Trivuelos";
            this.btnAerolineaAPI.UseVisualStyleBackColor = true;
            this.btnAerolineaAPI.Click += new System.EventHandler(this.ExtraerAerolinea);
            // 
            // dgAeropuerto
            // 
            this.dgAeropuerto.AutoGenerateColumns = false;
            this.dgAeropuerto.BackgroundColor = System.Drawing.Color.DimGray;
            this.dgAeropuerto.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgAeropuerto.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.fsDataGridViewTextBoxColumn,
            this.faaDataGridViewTextBoxColumn,
            this.iataDataGridViewTextBoxColumn,
            this.icaoDataGridViewTextBoxColumn,
            this.nombreDataGridViewTextBoxColumn,
            this.cuidadDataGridViewTextBoxColumn,
            this.codigoEstadoDataGridViewTextBoxColumn,
            this.codigoPaisDataGridViewTextBoxColumn,
            this.nombrePaisDataGridViewTextBoxColumn,
            this.nombreRegionDataGridViewTextBoxColumn,
            this.tiempoZonaRegionDataGridViewTextBoxColumn,
            this.latitudDataGridViewTextBoxColumn,
            this.longitudDataGridViewTextBoxColumn,
            this.elevacionPieDataGridViewTextBoxColumn,
            this.activoDataGridViewCheckBoxColumn,
            this.clasificacionDataGridViewTextBoxColumn});
            this.dgAeropuerto.DataSource = this.eAeropuertoBindingSource;
            this.dgAeropuerto.Location = new System.Drawing.Point(12, 144);
            this.dgAeropuerto.Name = "dgAeropuerto";
            this.dgAeropuerto.Size = new System.Drawing.Size(707, 179);
            this.dgAeropuerto.TabIndex = 3;
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn.FillWeight = 256.0241F;
            this.idDataGridViewTextBoxColumn.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.Visible = false;
            // 
            // fsDataGridViewTextBoxColumn
            // 
            this.fsDataGridViewTextBoxColumn.DataPropertyName = "Fs";
            this.fsDataGridViewTextBoxColumn.FillWeight = 218.948F;
            this.fsDataGridViewTextBoxColumn.HeaderText = "Fs";
            this.fsDataGridViewTextBoxColumn.Name = "fsDataGridViewTextBoxColumn";
            this.fsDataGridViewTextBoxColumn.Visible = false;
            this.fsDataGridViewTextBoxColumn.Width = 86;
            // 
            // faaDataGridViewTextBoxColumn
            // 
            this.faaDataGridViewTextBoxColumn.DataPropertyName = "Faa";
            this.faaDataGridViewTextBoxColumn.FillWeight = 187.4557F;
            this.faaDataGridViewTextBoxColumn.HeaderText = "Faa";
            this.faaDataGridViewTextBoxColumn.Name = "faaDataGridViewTextBoxColumn";
            this.faaDataGridViewTextBoxColumn.Visible = false;
            this.faaDataGridViewTextBoxColumn.Width = 73;
            // 
            // iataDataGridViewTextBoxColumn
            // 
            this.iataDataGridViewTextBoxColumn.DataPropertyName = "Iata";
            this.iataDataGridViewTextBoxColumn.FillWeight = 160.7062F;
            this.iataDataGridViewTextBoxColumn.HeaderText = "Iata";
            this.iataDataGridViewTextBoxColumn.Name = "iataDataGridViewTextBoxColumn";
            this.iataDataGridViewTextBoxColumn.Visible = false;
            this.iataDataGridViewTextBoxColumn.Width = 63;
            // 
            // icaoDataGridViewTextBoxColumn
            // 
            this.icaoDataGridViewTextBoxColumn.DataPropertyName = "Icao";
            this.icaoDataGridViewTextBoxColumn.FillWeight = 137.9853F;
            this.icaoDataGridViewTextBoxColumn.HeaderText = "Icao";
            this.icaoDataGridViewTextBoxColumn.Name = "icaoDataGridViewTextBoxColumn";
            this.icaoDataGridViewTextBoxColumn.Visible = false;
            this.icaoDataGridViewTextBoxColumn.Width = 53;
            // 
            // nombreDataGridViewTextBoxColumn
            // 
            this.nombreDataGridViewTextBoxColumn.DataPropertyName = "Nombre";
            this.nombreDataGridViewTextBoxColumn.FillWeight = 118.6862F;
            this.nombreDataGridViewTextBoxColumn.HeaderText = "Nombre";
            this.nombreDataGridViewTextBoxColumn.Name = "nombreDataGridViewTextBoxColumn";
            // 
            // cuidadDataGridViewTextBoxColumn
            // 
            this.cuidadDataGridViewTextBoxColumn.DataPropertyName = "Cuidad";
            this.cuidadDataGridViewTextBoxColumn.FillWeight = 102.2935F;
            this.cuidadDataGridViewTextBoxColumn.HeaderText = "Cuidad";
            this.cuidadDataGridViewTextBoxColumn.Name = "cuidadDataGridViewTextBoxColumn";
            // 
            // codigoEstadoDataGridViewTextBoxColumn
            // 
            this.codigoEstadoDataGridViewTextBoxColumn.DataPropertyName = "CodigoEstado";
            this.codigoEstadoDataGridViewTextBoxColumn.FillWeight = 88.36968F;
            this.codigoEstadoDataGridViewTextBoxColumn.HeaderText = "CodigoEstado";
            this.codigoEstadoDataGridViewTextBoxColumn.Name = "codigoEstadoDataGridViewTextBoxColumn";
            // 
            // codigoPaisDataGridViewTextBoxColumn
            // 
            this.codigoPaisDataGridViewTextBoxColumn.DataPropertyName = "CodigoPais";
            this.codigoPaisDataGridViewTextBoxColumn.FillWeight = 76.54279F;
            this.codigoPaisDataGridViewTextBoxColumn.HeaderText = "CodigoPais";
            this.codigoPaisDataGridViewTextBoxColumn.Name = "codigoPaisDataGridViewTextBoxColumn";
            // 
            // nombrePaisDataGridViewTextBoxColumn
            // 
            this.nombrePaisDataGridViewTextBoxColumn.DataPropertyName = "NombrePais";
            this.nombrePaisDataGridViewTextBoxColumn.FillWeight = 66.49706F;
            this.nombrePaisDataGridViewTextBoxColumn.HeaderText = "NombrePais";
            this.nombrePaisDataGridViewTextBoxColumn.Name = "nombrePaisDataGridViewTextBoxColumn";
            // 
            // nombreRegionDataGridViewTextBoxColumn
            // 
            this.nombreRegionDataGridViewTextBoxColumn.DataPropertyName = "NombreRegion";
            this.nombreRegionDataGridViewTextBoxColumn.FillWeight = 57.96424F;
            this.nombreRegionDataGridViewTextBoxColumn.HeaderText = "NombreRegion";
            this.nombreRegionDataGridViewTextBoxColumn.Name = "nombreRegionDataGridViewTextBoxColumn";
            // 
            // tiempoZonaRegionDataGridViewTextBoxColumn
            // 
            this.tiempoZonaRegionDataGridViewTextBoxColumn.DataPropertyName = "TiempoZonaRegion";
            this.tiempoZonaRegionDataGridViewTextBoxColumn.FillWeight = 50.71648F;
            this.tiempoZonaRegionDataGridViewTextBoxColumn.HeaderText = "TiempoZonaRegion";
            this.tiempoZonaRegionDataGridViewTextBoxColumn.Name = "tiempoZonaRegionDataGridViewTextBoxColumn";
            // 
            // latitudDataGridViewTextBoxColumn
            // 
            this.latitudDataGridViewTextBoxColumn.DataPropertyName = "Latitud";
            this.latitudDataGridViewTextBoxColumn.FillWeight = 44.56026F;
            this.latitudDataGridViewTextBoxColumn.HeaderText = "Latitud";
            this.latitudDataGridViewTextBoxColumn.Name = "latitudDataGridViewTextBoxColumn";
            // 
            // longitudDataGridViewTextBoxColumn
            // 
            this.longitudDataGridViewTextBoxColumn.DataPropertyName = "Longitud";
            this.longitudDataGridViewTextBoxColumn.FillWeight = 39.33119F;
            this.longitudDataGridViewTextBoxColumn.HeaderText = "Longitud";
            this.longitudDataGridViewTextBoxColumn.Name = "longitudDataGridViewTextBoxColumn";
            // 
            // elevacionPieDataGridViewTextBoxColumn
            // 
            this.elevacionPieDataGridViewTextBoxColumn.DataPropertyName = "ElevacionPie";
            this.elevacionPieDataGridViewTextBoxColumn.FillWeight = 34.88961F;
            this.elevacionPieDataGridViewTextBoxColumn.HeaderText = "ElevacionPie";
            this.elevacionPieDataGridViewTextBoxColumn.Name = "elevacionPieDataGridViewTextBoxColumn";
            // 
            // activoDataGridViewCheckBoxColumn
            // 
            this.activoDataGridViewCheckBoxColumn.DataPropertyName = "Activo";
            this.activoDataGridViewCheckBoxColumn.FillWeight = 31.11696F;
            this.activoDataGridViewCheckBoxColumn.HeaderText = "Activo";
            this.activoDataGridViewCheckBoxColumn.Name = "activoDataGridViewCheckBoxColumn";
            // 
            // clasificacionDataGridViewTextBoxColumn
            // 
            this.clasificacionDataGridViewTextBoxColumn.DataPropertyName = "Clasificacion";
            this.clasificacionDataGridViewTextBoxColumn.FillWeight = 27.91246F;
            this.clasificacionDataGridViewTextBoxColumn.HeaderText = "Clasificacion";
            this.clasificacionDataGridViewTextBoxColumn.Name = "clasificacionDataGridViewTextBoxColumn";
            // 
            // eAeropuertoBindingSource
            // 
            this.eAeropuertoBindingSource.DataSource = typeof(Trivuelosentities.EAeropuerto);
            // 
            // dgAerolinea
            // 
            this.dgAerolinea.AutoGenerateColumns = false;
            this.dgAerolinea.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgAerolinea.BackgroundColor = System.Drawing.Color.DimGray;
            this.dgAerolinea.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgAerolinea.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn1,
            this.fsDataGridViewTextBoxColumn1,
            this.iataDataGridViewTextBoxColumn1,
            this.icaoDataGridViewTextBoxColumn1,
            this.nombreDataGridViewTextBoxColumn1,
            this.activoDataGridViewCheckBoxColumn1});
            this.dgAerolinea.DataSource = this.eAerolineaBindingSource;
            this.dgAerolinea.Location = new System.Drawing.Point(6, 463);
            this.dgAerolinea.Name = "dgAerolinea";
            this.dgAerolinea.Size = new System.Drawing.Size(716, 182);
            this.dgAerolinea.TabIndex = 4;
            // 
            // idDataGridViewTextBoxColumn1
            // 
            this.idDataGridViewTextBoxColumn1.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn1.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn1.Name = "idDataGridViewTextBoxColumn1";
            // 
            // fsDataGridViewTextBoxColumn1
            // 
            this.fsDataGridViewTextBoxColumn1.DataPropertyName = "Fs";
            this.fsDataGridViewTextBoxColumn1.HeaderText = "Fs";
            this.fsDataGridViewTextBoxColumn1.Name = "fsDataGridViewTextBoxColumn1";
            // 
            // iataDataGridViewTextBoxColumn1
            // 
            this.iataDataGridViewTextBoxColumn1.DataPropertyName = "Iata";
            this.iataDataGridViewTextBoxColumn1.HeaderText = "Iata";
            this.iataDataGridViewTextBoxColumn1.Name = "iataDataGridViewTextBoxColumn1";
            // 
            // icaoDataGridViewTextBoxColumn1
            // 
            this.icaoDataGridViewTextBoxColumn1.DataPropertyName = "Icao";
            this.icaoDataGridViewTextBoxColumn1.HeaderText = "Icao";
            this.icaoDataGridViewTextBoxColumn1.Name = "icaoDataGridViewTextBoxColumn1";
            // 
            // nombreDataGridViewTextBoxColumn1
            // 
            this.nombreDataGridViewTextBoxColumn1.DataPropertyName = "Nombre";
            this.nombreDataGridViewTextBoxColumn1.HeaderText = "Nombre";
            this.nombreDataGridViewTextBoxColumn1.Name = "nombreDataGridViewTextBoxColumn1";
            // 
            // activoDataGridViewCheckBoxColumn1
            // 
            this.activoDataGridViewCheckBoxColumn1.DataPropertyName = "Activo";
            this.activoDataGridViewCheckBoxColumn1.HeaderText = "Activo";
            this.activoDataGridViewCheckBoxColumn1.Name = "activoDataGridViewCheckBoxColumn1";
            // 
            // eAerolineaBindingSource
            // 
            this.eAerolineaBindingSource.DataSource = typeof(Trivuelosentities.EAerolinea);
            // 
            // btnAeropuertoDB
            // 
            this.btnAeropuertoDB.Location = new System.Drawing.Point(274, 50);
            this.btnAeropuertoDB.Name = "btnAeropuertoDB";
            this.btnAeropuertoDB.Size = new System.Drawing.Size(172, 39);
            this.btnAeropuertoDB.TabIndex = 5;
            this.btnAeropuertoDB.Text = "Cargar Aeropuertos de Trivuelos";
            this.btnAeropuertoDB.UseVisualStyleBackColor = true;
            this.btnAeropuertoDB.Click += new System.EventHandler(this.CargarAeropuerto);
            // 
            // btnAerolineaBD
            // 
            this.btnAerolineaBD.Location = new System.Drawing.Point(274, 382);
            this.btnAerolineaBD.Name = "btnAerolineaBD";
            this.btnAerolineaBD.Size = new System.Drawing.Size(172, 39);
            this.btnAerolineaBD.TabIndex = 6;
            this.btnAerolineaBD.Text = "Cargar Aerolineas de Trivuelos";
            this.btnAerolineaBD.UseVisualStyleBackColor = true;
            this.btnAerolineaBD.Click += new System.EventHandler(this.CargarAerolinea);
            // 
            // pbAerolinea
            // 
            this.pbAerolinea.Location = new System.Drawing.Point(45, 427);
            this.pbAerolinea.Name = "pbAerolinea";
            this.pbAerolinea.Size = new System.Drawing.Size(172, 23);
            this.pbAerolinea.Step = 100;
            this.pbAerolinea.TabIndex = 7;
            // 
            // pbAeropuerto
            // 
            this.pbAeropuerto.Location = new System.Drawing.Point(45, 96);
            this.pbAeropuerto.Name = "pbAeropuerto";
            this.pbAeropuerto.Size = new System.Drawing.Size(172, 23);
            this.pbAeropuerto.Step = 100;
            this.pbAeropuerto.TabIndex = 8;
            // 
            // tAeropuerto
            // 
            this.tAeropuerto.Interval = 2000;
            this.tAeropuerto.Tick += new System.EventHandler(this.TiempoPuerto);
            // 
            // tAerolinea
            // 
            this.tAerolinea.Interval = 2000;
            this.tAerolinea.Tick += new System.EventHandler(this.TiempoLinea);
            // 
            // EliminarAero
            // 
            this.EliminarAero.Location = new System.Drawing.Point(512, 50);
            this.EliminarAero.Name = "EliminarAero";
            this.EliminarAero.Size = new System.Drawing.Size(172, 39);
            this.EliminarAero.TabIndex = 9;
            this.EliminarAero.Text = "Eliminar aeropuerto";
            this.EliminarAero.UseVisualStyleBackColor = true;
            this.EliminarAero.Click += new System.EventHandler(this.button1_Click);
            // 
            // EliminarLinea
            // 
            this.EliminarLinea.Location = new System.Drawing.Point(512, 382);
            this.EliminarLinea.Name = "EliminarLinea";
            this.EliminarLinea.Size = new System.Drawing.Size(172, 39);
            this.EliminarLinea.TabIndex = 10;
            this.EliminarLinea.Text = "Eliminar aerolinea";
            this.EliminarLinea.UseVisualStyleBackColor = true;
            this.EliminarLinea.Click += new System.EventHandler(this.EliminarLinea_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(263, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(209, 18);
            this.label1.TabIndex = 11;
            this.label1.Text = "Datos de los aeropuertos";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(252, 344);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(194, 18);
            this.label2.TabIndex = 12;
            this.label2.Text = "Datos de las aerolíneas";
            // 
            // Prueba
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(97)))), ((int)(((byte)(97)))), ((int)(((byte)(97)))));
            this.ClientSize = new System.Drawing.Size(731, 655);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.EliminarLinea);
            this.Controls.Add(this.EliminarAero);
            this.Controls.Add(this.pbAeropuerto);
            this.Controls.Add(this.pbAerolinea);
            this.Controls.Add(this.btnAerolineaBD);
            this.Controls.Add(this.btnAeropuertoDB);
            this.Controls.Add(this.dgAerolinea);
            this.Controls.Add(this.dgAeropuerto);
            this.Controls.Add(this.btnAerolineaAPI);
            this.Controls.Add(this.btnAeropuertoAPI);
            this.Name = "Prueba";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Prueba";
            this.Load += new System.EventHandler(this.Prueba_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgAeropuerto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eAeropuertoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgAerolinea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eAerolineaBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAeropuertoAPI;
        private System.Windows.Forms.Button btnAerolineaAPI;
        private System.Windows.Forms.DataGridView dgAeropuerto;
        private System.Windows.Forms.DataGridView dgAerolinea;
        private System.Windows.Forms.Button btnAeropuertoDB;
        private System.Windows.Forms.Button btnAerolineaBD;
        private System.Windows.Forms.BindingSource eAeropuertoBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn fsDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn iataDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn icaoDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombreDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn activoDataGridViewCheckBoxColumn1;
        private System.Windows.Forms.BindingSource eAerolineaBindingSource;
        private System.Windows.Forms.ProgressBar pbAerolinea;
        private System.Windows.Forms.ProgressBar pbAeropuerto;
        private System.Windows.Forms.Timer tAeropuerto;
        private System.Windows.Forms.Timer tAerolinea;
        private System.Windows.Forms.Button EliminarAero;
        private System.Windows.Forms.Button EliminarLinea;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn faaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn iataDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn icaoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombreDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cuidadDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn codigoEstadoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn codigoPaisDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombrePaisDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombreRegionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tiempoZonaRegionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn latitudDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn longitudDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn elevacionPieDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn activoDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn clasificacionDataGridViewTextBoxColumn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}