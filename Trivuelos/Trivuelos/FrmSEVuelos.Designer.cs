﻿namespace Trivuelos
{
    partial class FrmSEVuelos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label5 = new System.Windows.Forms.Label();
            this.nupHora = new System.Windows.Forms.NumericUpDown();
            this.btnLlegada = new System.Windows.Forms.Button();
            this.dgVuelos = new System.Windows.Forms.DataGridView();
            this.salidaAeropuertoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.horaSalidaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.horaLlegadaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.llegadaAeropuertoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.portadorDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numeroVueloDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.paradaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.eVueloBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.btnSalida = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.cbxAeropuerto = new System.Windows.Forms.ComboBox();
            this.eAeropuertoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dtpFecha = new System.Windows.Forms.DateTimePicker();
            this.btnRuta = new System.Windows.Forms.Button();
            this.lbInformacion = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.nupHora)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgVuelos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eVueloBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eAeropuertoBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(645, 76);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 17);
            this.label5.TabIndex = 17;
            this.label5.Text = "Hora";
            // 
            // nupHora
            // 
            this.nupHora.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.nupHora.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nupHora.Location = new System.Drawing.Point(649, 100);
            this.nupHora.Maximum = new decimal(new int[] {
            24,
            0,
            0,
            0});
            this.nupHora.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nupHora.Name = "nupHora";
            this.nupHora.Size = new System.Drawing.Size(49, 25);
            this.nupHora.TabIndex = 9;
            this.nupHora.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // btnLlegada
            // 
            this.btnLlegada.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(73)))), ((int)(((byte)(94)))));
            this.btnLlegada.FlatAppearance.BorderSize = 0;
            this.btnLlegada.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLlegada.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLlegada.ForeColor = System.Drawing.Color.White;
            this.btnLlegada.Location = new System.Drawing.Point(83, 149);
            this.btnLlegada.Name = "btnLlegada";
            this.btnLlegada.Size = new System.Drawing.Size(171, 27);
            this.btnLlegada.TabIndex = 16;
            this.btnLlegada.Text = "Buscar por llegada";
            this.btnLlegada.UseVisualStyleBackColor = false;
            this.btnLlegada.Click += new System.EventHandler(this.BuscarLlegada);
            // 
            // dgVuelos
            // 
            this.dgVuelos.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.dgVuelos.AutoGenerateColumns = false;
            this.dgVuelos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgVuelos.BackgroundColor = System.Drawing.Color.DimGray;
            this.dgVuelos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgVuelos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.salidaAeropuertoDataGridViewTextBoxColumn,
            this.horaSalidaDataGridViewTextBoxColumn,
            this.horaLlegadaDataGridViewTextBoxColumn,
            this.llegadaAeropuertoDataGridViewTextBoxColumn,
            this.portadorDataGridViewTextBoxColumn,
            this.numeroVueloDataGridViewTextBoxColumn,
            this.paradaDataGridViewTextBoxColumn});
            this.dgVuelos.DataSource = this.eVueloBindingSource;
            this.dgVuelos.Location = new System.Drawing.Point(10, 182);
            this.dgVuelos.Name = "dgVuelos";
            this.dgVuelos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgVuelos.Size = new System.Drawing.Size(794, 234);
            this.dgVuelos.TabIndex = 15;
            // 
            // salidaAeropuertoDataGridViewTextBoxColumn
            // 
            this.salidaAeropuertoDataGridViewTextBoxColumn.DataPropertyName = "SalidaAeropuerto";
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.White;
            this.salidaAeropuertoDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle8;
            this.salidaAeropuertoDataGridViewTextBoxColumn.HeaderText = "Salida del aeropuerto";
            this.salidaAeropuertoDataGridViewTextBoxColumn.Name = "salidaAeropuertoDataGridViewTextBoxColumn";
            // 
            // horaSalidaDataGridViewTextBoxColumn
            // 
            this.horaSalidaDataGridViewTextBoxColumn.DataPropertyName = "HoraSalida";
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.White;
            this.horaSalidaDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle9;
            this.horaSalidaDataGridViewTextBoxColumn.HeaderText = "Hora de salida";
            this.horaSalidaDataGridViewTextBoxColumn.Name = "horaSalidaDataGridViewTextBoxColumn";
            // 
            // horaLlegadaDataGridViewTextBoxColumn
            // 
            this.horaLlegadaDataGridViewTextBoxColumn.DataPropertyName = "HoraLlegada";
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.White;
            this.horaLlegadaDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle10;
            this.horaLlegadaDataGridViewTextBoxColumn.HeaderText = "Hora de llegada";
            this.horaLlegadaDataGridViewTextBoxColumn.Name = "horaLlegadaDataGridViewTextBoxColumn";
            // 
            // llegadaAeropuertoDataGridViewTextBoxColumn
            // 
            this.llegadaAeropuertoDataGridViewTextBoxColumn.DataPropertyName = "LlegadaAeropuerto";
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle11.ForeColor = System.Drawing.Color.White;
            this.llegadaAeropuertoDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle11;
            this.llegadaAeropuertoDataGridViewTextBoxColumn.HeaderText = "Destino aeropuerto";
            this.llegadaAeropuertoDataGridViewTextBoxColumn.Name = "llegadaAeropuertoDataGridViewTextBoxColumn";
            // 
            // portadorDataGridViewTextBoxColumn
            // 
            this.portadorDataGridViewTextBoxColumn.DataPropertyName = "Portador";
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle12.ForeColor = System.Drawing.Color.White;
            this.portadorDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle12;
            this.portadorDataGridViewTextBoxColumn.HeaderText = "Aerolínea";
            this.portadorDataGridViewTextBoxColumn.Name = "portadorDataGridViewTextBoxColumn";
            // 
            // numeroVueloDataGridViewTextBoxColumn
            // 
            this.numeroVueloDataGridViewTextBoxColumn.DataPropertyName = "NumeroVuelo";
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle13.ForeColor = System.Drawing.Color.White;
            this.numeroVueloDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle13;
            this.numeroVueloDataGridViewTextBoxColumn.HeaderText = "Numero de vuelo";
            this.numeroVueloDataGridViewTextBoxColumn.Name = "numeroVueloDataGridViewTextBoxColumn";
            // 
            // paradaDataGridViewTextBoxColumn
            // 
            this.paradaDataGridViewTextBoxColumn.DataPropertyName = "Parada";
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle14.ForeColor = System.Drawing.Color.White;
            this.paradaDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle14;
            this.paradaDataGridViewTextBoxColumn.HeaderText = "Parada";
            this.paradaDataGridViewTextBoxColumn.Name = "paradaDataGridViewTextBoxColumn";
            // 
            // eVueloBindingSource
            // 
            this.eVueloBindingSource.DataSource = typeof(Trivuelosentities.EVuelo);
            // 
            // btnSalida
            // 
            this.btnSalida.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSalida.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(73)))), ((int)(((byte)(94)))));
            this.btnSalida.FlatAppearance.BorderSize = 0;
            this.btnSalida.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSalida.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalida.ForeColor = System.Drawing.Color.White;
            this.btnSalida.Location = new System.Drawing.Point(554, 149);
            this.btnSalida.Name = "btnSalida";
            this.btnSalida.Size = new System.Drawing.Size(170, 27);
            this.btnSalida.TabIndex = 14;
            this.btnSalida.Text = "Buscar por salida";
            this.btnSalida.UseVisualStyleBackColor = false;
            this.btnSalida.Click += new System.EventHandler(this.BuscarSalida);
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(315, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(189, 18);
            this.label6.TabIndex = 10;
            this.label6.Text = "Vuelos por Aeropuerto";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(399, 76);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 17);
            this.label7.TabIndex = 13;
            this.label7.Text = "Fecha";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(85, 76);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(92, 17);
            this.label8.TabIndex = 12;
            this.label8.Text = "Aeropuerto";
            // 
            // cbxAeropuerto
            // 
            this.cbxAeropuerto.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.eAeropuertoBindingSource, "Fs", true));
            this.cbxAeropuerto.DataSource = this.eAeropuertoBindingSource;
            this.cbxAeropuerto.DisplayMember = "Nombre";
            this.cbxAeropuerto.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxAeropuerto.FormattingEnabled = true;
            this.cbxAeropuerto.Location = new System.Drawing.Point(48, 100);
            this.cbxAeropuerto.Name = "cbxAeropuerto";
            this.cbxAeropuerto.Size = new System.Drawing.Size(180, 25);
            this.cbxAeropuerto.TabIndex = 11;
            this.cbxAeropuerto.ValueMember = "Fs";
            // 
            // eAeropuertoBindingSource
            // 
            this.eAeropuertoBindingSource.DataSource = typeof(Trivuelosentities.EAeropuerto);
            // 
            // dtpFecha
            // 
            this.dtpFecha.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dtpFecha.CalendarForeColor = System.Drawing.Color.White;
            this.dtpFecha.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpFecha.Location = new System.Drawing.Point(274, 100);
            this.dtpFecha.Name = "dtpFecha";
            this.dtpFecha.Size = new System.Drawing.Size(296, 25);
            this.dtpFecha.TabIndex = 8;
            // 
            // btnRuta
            // 
            this.btnRuta.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(73)))), ((int)(((byte)(94)))));
            this.btnRuta.FlatAppearance.BorderSize = 0;
            this.btnRuta.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRuta.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRuta.ForeColor = System.Drawing.Color.White;
            this.btnRuta.Location = new System.Drawing.Point(333, 149);
            this.btnRuta.Name = "btnRuta";
            this.btnRuta.Size = new System.Drawing.Size(171, 27);
            this.btnRuta.TabIndex = 18;
            this.btnRuta.Text = "Ver ruta";
            this.btnRuta.UseVisualStyleBackColor = false;
            this.btnRuta.Click += new System.EventHandler(this.VerRuta);
            // 
            // lbInformacion
            // 
            this.lbInformacion.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbInformacion.Location = new System.Drawing.Point(12, 9);
            this.lbInformacion.Name = "lbInformacion";
            this.lbInformacion.Size = new System.Drawing.Size(271, 67);
            this.lbInformacion.TabIndex = 19;
            this.lbInformacion.Text = "Información";
            this.lbInformacion.Visible = false;
            // 
            // FrmSEVuelos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(97)))), ((int)(((byte)(97)))), ((int)(((byte)(97)))));
            this.ClientSize = new System.Drawing.Size(816, 424);
            this.Controls.Add(this.lbInformacion);
            this.Controls.Add(this.btnRuta);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.nupHora);
            this.Controls.Add(this.btnLlegada);
            this.Controls.Add(this.dgVuelos);
            this.Controls.Add(this.btnSalida);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.cbxAeropuerto);
            this.Controls.Add(this.dtpFecha);
            this.ForeColor = System.Drawing.Color.White;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmSEVuelos";
            this.Text = "FrmSEVuelos";
            this.Load += new System.EventHandler(this.FrmSEVuelos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nupHora)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgVuelos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eVueloBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eAeropuertoBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown nupHora;
        private System.Windows.Forms.Button btnLlegada;
        private System.Windows.Forms.DataGridView dgVuelos;
        private System.Windows.Forms.Button btnSalida;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cbxAeropuerto;
        private System.Windows.Forms.DateTimePicker dtpFecha;
        private System.Windows.Forms.BindingSource eAeropuertoBindingSource;
        private System.Windows.Forms.BindingSource eVueloBindingSource;
        private System.Windows.Forms.Button btnRuta;
        private System.Windows.Forms.DataGridViewTextBoxColumn salidaAeropuertoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn horaSalidaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn horaLlegadaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn llegadaAeropuertoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn portadorDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn numeroVueloDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn paradaDataGridViewTextBoxColumn;
        private System.Windows.Forms.Label lbInformacion;
    }
}