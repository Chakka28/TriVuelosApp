﻿namespace Trivuelos
{
    partial class FrmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblNombre = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnVuelo = new System.Windows.Forms.Button();
            this.btnRetrasos = new System.Windows.Forms.Button();
            this.btnBusquedaVuelo = new System.Windows.Forms.Button();
            this.btnEstadoVuelo = new System.Windows.Forms.Button();
            this.btnCargarAeropuertos = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pbfoto = new System.Windows.Forms.PictureBox();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbfoto)).BeginInit();
            this.SuspendLayout();
            // 
            // lblNombre
            // 
            this.lblNombre.BackColor = System.Drawing.Color.IndianRed;
            this.lblNombre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombre.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblNombre.Location = new System.Drawing.Point(497, 72);
            this.lblNombre.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(76, 19);
            this.lblNombre.TabIndex = 2;
            this.lblNombre.Text = "Keilor Rivera Rojas";
            this.lblNombre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.DimGray;
            this.label1.Location = new System.Drawing.Point(-1, 98);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(584, 8);
            this.label1.TabIndex = 7;
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.IndianRed;
            this.panel2.Controls.Add(this.btnVuelo);
            this.panel2.Controls.Add(this.btnRetrasos);
            this.panel2.Controls.Add(this.btnBusquedaVuelo);
            this.panel2.Controls.Add(this.btnEstadoVuelo);
            this.panel2.Controls.Add(this.btnCargarAeropuertos);
            this.panel2.Location = new System.Drawing.Point(9, 108);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(563, 334);
            this.panel2.TabIndex = 8;
            // 
            // btnVuelo
            // 
            this.btnVuelo.BackColor = System.Drawing.Color.LightGray;
            this.btnVuelo.BackgroundImage = global::Trivuelos.Properties.Resources.button;
            this.btnVuelo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnVuelo.Location = new System.Drawing.Point(334, 225);
            this.btnVuelo.Margin = new System.Windows.Forms.Padding(2);
            this.btnVuelo.Name = "btnVuelo";
            this.btnVuelo.Size = new System.Drawing.Size(204, 58);
            this.btnVuelo.TabIndex = 8;
            this.btnVuelo.UseVisualStyleBackColor = false;
            this.btnVuelo.Click += new System.EventHandler(this.SeguimientoVuelo);
            // 
            // btnRetrasos
            // 
            this.btnRetrasos.BackColor = System.Drawing.Color.LightGray;
            this.btnRetrasos.BackgroundImage = global::Trivuelos.Properties.Resources.button_retasos;
            this.btnRetrasos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRetrasos.Location = new System.Drawing.Point(334, 114);
            this.btnRetrasos.Margin = new System.Windows.Forms.Padding(2);
            this.btnRetrasos.Name = "btnRetrasos";
            this.btnRetrasos.Size = new System.Drawing.Size(204, 58);
            this.btnRetrasos.TabIndex = 7;
            this.btnRetrasos.UseVisualStyleBackColor = false;
            this.btnRetrasos.Click += new System.EventHandler(this.btnRetrasos_Click);
            // 
            // btnBusquedaVuelo
            // 
            this.btnBusquedaVuelo.BackColor = System.Drawing.Color.LightGray;
            this.btnBusquedaVuelo.BackgroundImage = global::Trivuelos.Properties.Resources.button_seguimiento;
            this.btnBusquedaVuelo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnBusquedaVuelo.Location = new System.Drawing.Point(18, 225);
            this.btnBusquedaVuelo.Margin = new System.Windows.Forms.Padding(2);
            this.btnBusquedaVuelo.Name = "btnBusquedaVuelo";
            this.btnBusquedaVuelo.Size = new System.Drawing.Size(199, 58);
            this.btnBusquedaVuelo.TabIndex = 6;
            this.btnBusquedaVuelo.UseVisualStyleBackColor = false;
            this.btnBusquedaVuelo.Click += new System.EventHandler(this.BusquedaVuelo);
            // 
            // btnEstadoVuelo
            // 
            this.btnEstadoVuelo.BackColor = System.Drawing.Color.LightGray;
            this.btnEstadoVuelo.BackgroundImage = global::Trivuelos.Properties.Resources.button_estado;
            this.btnEstadoVuelo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnEstadoVuelo.Location = new System.Drawing.Point(18, 114);
            this.btnEstadoVuelo.Margin = new System.Windows.Forms.Padding(2);
            this.btnEstadoVuelo.Name = "btnEstadoVuelo";
            this.btnEstadoVuelo.Size = new System.Drawing.Size(199, 58);
            this.btnEstadoVuelo.TabIndex = 5;
            this.btnEstadoVuelo.UseVisualStyleBackColor = false;
            this.btnEstadoVuelo.Click += new System.EventHandler(this.btnEstadoVuelo_Click);
            // 
            // btnCargarAeropuertos
            // 
            this.btnCargarAeropuertos.BackColor = System.Drawing.Color.LightGray;
            this.btnCargarAeropuertos.BackgroundImage = global::Trivuelos.Properties.Resources.button_mant;
            this.btnCargarAeropuertos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCargarAeropuertos.Location = new System.Drawing.Point(139, 23);
            this.btnCargarAeropuertos.Margin = new System.Windows.Forms.Padding(2);
            this.btnCargarAeropuertos.Name = "btnCargarAeropuertos";
            this.btnCargarAeropuertos.Size = new System.Drawing.Size(280, 58);
            this.btnCargarAeropuertos.TabIndex = 4;
            this.btnCargarAeropuertos.UseVisualStyleBackColor = false;
            this.btnCargarAeropuertos.Click += new System.EventHandler(this.btnCargarAeropuertos_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Trivuelos.Properties.Resources._3;
            this.pictureBox1.Location = new System.Drawing.Point(2, 19);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(484, 63);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // pbfoto
            // 
            this.pbfoto.BackColor = System.Drawing.Color.White;
            this.pbfoto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbfoto.Image = global::Trivuelos.Properties.Resources.if_user_male2_172626;
            this.pbfoto.Location = new System.Drawing.Point(497, 7);
            this.pbfoto.Margin = new System.Windows.Forms.Padding(2);
            this.pbfoto.Name = "pbfoto";
            this.pbfoto.Size = new System.Drawing.Size(76, 62);
            this.pbfoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbfoto.TabIndex = 1;
            this.pbfoto.TabStop = false;
            // 
            // FrmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightGray;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(581, 446);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lblNombre);
            this.Controls.Add(this.pbfoto);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FrmPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmPrincipal";
            this.Load += new System.EventHandler(this.FrmPrincipal_Load);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbfoto)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.PictureBox pbfoto;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnCargarAeropuertos;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnEstadoVuelo;
        private System.Windows.Forms.Button btnBusquedaVuelo;
        private System.Windows.Forms.Button btnRetrasos;
        private System.Windows.Forms.Button btnVuelo;
    }
}