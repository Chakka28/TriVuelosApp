﻿namespace Trivuelos
{
    partial class FrmBusquedaN4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.cbregiones = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dgAero = new System.Windows.Forms.DataGridView();
            this.fsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iataDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.icaoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.faaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.street1DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.street2DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cityDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cityCodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.districtDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stateCodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.postalCodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.countryCodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.countryNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.regionNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.timeZoneRegionNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.weatherZoneDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.localTimeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.utcOffsetHoursDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.latitudeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.longitudeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.elevationFeetDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.elevationFeetSpecifiedDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.classificationDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.activeDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dateFromDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateToDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.delayIndexUrlDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.weatherUrlDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.airportV1BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.panel5 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.dgAero)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.airportV1BindingSource)).BeginInit();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // cbregiones
            // 
            this.cbregiones.FormattingEnabled = true;
            this.cbregiones.Items.AddRange(new object[] {
            "Africa",
            "Antarctica",
            "Caribbean",
            "Central-America",
            "Europe",
            "Middle-East",
            "North-America",
            "Oceania",
            "South-America",
            "Asia"});
            this.cbregiones.Location = new System.Drawing.Point(64, 26);
            this.cbregiones.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbregiones.Name = "cbregiones";
            this.cbregiones.Size = new System.Drawing.Size(92, 21);
            this.cbregiones.TabIndex = 0;
            this.cbregiones.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 26);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Regiones";
            // 
            // dgAero
            // 
            this.dgAero.AutoGenerateColumns = false;
            this.dgAero.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgAero.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fsDataGridViewTextBoxColumn,
            this.iataDataGridViewTextBoxColumn,
            this.icaoDataGridViewTextBoxColumn,
            this.faaDataGridViewTextBoxColumn,
            this.nameDataGridViewTextBoxColumn,
            this.street1DataGridViewTextBoxColumn,
            this.street2DataGridViewTextBoxColumn,
            this.cityDataGridViewTextBoxColumn,
            this.cityCodeDataGridViewTextBoxColumn,
            this.districtDataGridViewTextBoxColumn,
            this.stateCodeDataGridViewTextBoxColumn,
            this.postalCodeDataGridViewTextBoxColumn,
            this.countryCodeDataGridViewTextBoxColumn,
            this.countryNameDataGridViewTextBoxColumn,
            this.regionNameDataGridViewTextBoxColumn,
            this.timeZoneRegionNameDataGridViewTextBoxColumn,
            this.weatherZoneDataGridViewTextBoxColumn,
            this.localTimeDataGridViewTextBoxColumn,
            this.utcOffsetHoursDataGridViewTextBoxColumn,
            this.latitudeDataGridViewTextBoxColumn,
            this.longitudeDataGridViewTextBoxColumn,
            this.elevationFeetDataGridViewTextBoxColumn,
            this.elevationFeetSpecifiedDataGridViewCheckBoxColumn,
            this.classificationDataGridViewTextBoxColumn,
            this.activeDataGridViewCheckBoxColumn,
            this.dateFromDataGridViewTextBoxColumn,
            this.dateToDataGridViewTextBoxColumn,
            this.delayIndexUrlDataGridViewTextBoxColumn,
            this.weatherUrlDataGridViewTextBoxColumn});
            this.dgAero.DataSource = this.airportV1BindingSource;
            this.dgAero.Location = new System.Drawing.Point(16, 15);
            this.dgAero.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dgAero.Name = "dgAero";
            this.dgAero.RowTemplate.Height = 24;
            this.dgAero.Size = new System.Drawing.Size(451, 171);
            this.dgAero.TabIndex = 2;
            // 
            // fsDataGridViewTextBoxColumn
            // 
            this.fsDataGridViewTextBoxColumn.DataPropertyName = "fs";
            this.fsDataGridViewTextBoxColumn.HeaderText = "fs";
            this.fsDataGridViewTextBoxColumn.Name = "fsDataGridViewTextBoxColumn";
            this.fsDataGridViewTextBoxColumn.Visible = false;
            // 
            // iataDataGridViewTextBoxColumn
            // 
            this.iataDataGridViewTextBoxColumn.DataPropertyName = "iata";
            this.iataDataGridViewTextBoxColumn.HeaderText = "iata";
            this.iataDataGridViewTextBoxColumn.Name = "iataDataGridViewTextBoxColumn";
            this.iataDataGridViewTextBoxColumn.Visible = false;
            // 
            // icaoDataGridViewTextBoxColumn
            // 
            this.icaoDataGridViewTextBoxColumn.DataPropertyName = "icao";
            this.icaoDataGridViewTextBoxColumn.HeaderText = "icao";
            this.icaoDataGridViewTextBoxColumn.Name = "icaoDataGridViewTextBoxColumn";
            this.icaoDataGridViewTextBoxColumn.Visible = false;
            // 
            // faaDataGridViewTextBoxColumn
            // 
            this.faaDataGridViewTextBoxColumn.DataPropertyName = "faa";
            this.faaDataGridViewTextBoxColumn.HeaderText = "faa";
            this.faaDataGridViewTextBoxColumn.Name = "faaDataGridViewTextBoxColumn";
            this.faaDataGridViewTextBoxColumn.Visible = false;
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "name";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            // 
            // street1DataGridViewTextBoxColumn
            // 
            this.street1DataGridViewTextBoxColumn.DataPropertyName = "street1";
            this.street1DataGridViewTextBoxColumn.HeaderText = "street1";
            this.street1DataGridViewTextBoxColumn.Name = "street1DataGridViewTextBoxColumn";
            this.street1DataGridViewTextBoxColumn.Visible = false;
            // 
            // street2DataGridViewTextBoxColumn
            // 
            this.street2DataGridViewTextBoxColumn.DataPropertyName = "street2";
            this.street2DataGridViewTextBoxColumn.HeaderText = "street2";
            this.street2DataGridViewTextBoxColumn.Name = "street2DataGridViewTextBoxColumn";
            this.street2DataGridViewTextBoxColumn.Visible = false;
            // 
            // cityDataGridViewTextBoxColumn
            // 
            this.cityDataGridViewTextBoxColumn.DataPropertyName = "city";
            this.cityDataGridViewTextBoxColumn.HeaderText = "city";
            this.cityDataGridViewTextBoxColumn.Name = "cityDataGridViewTextBoxColumn";
            // 
            // cityCodeDataGridViewTextBoxColumn
            // 
            this.cityCodeDataGridViewTextBoxColumn.DataPropertyName = "cityCode";
            this.cityCodeDataGridViewTextBoxColumn.HeaderText = "cityCode";
            this.cityCodeDataGridViewTextBoxColumn.Name = "cityCodeDataGridViewTextBoxColumn";
            this.cityCodeDataGridViewTextBoxColumn.Visible = false;
            // 
            // districtDataGridViewTextBoxColumn
            // 
            this.districtDataGridViewTextBoxColumn.DataPropertyName = "district";
            this.districtDataGridViewTextBoxColumn.HeaderText = "district";
            this.districtDataGridViewTextBoxColumn.Name = "districtDataGridViewTextBoxColumn";
            this.districtDataGridViewTextBoxColumn.Visible = false;
            // 
            // stateCodeDataGridViewTextBoxColumn
            // 
            this.stateCodeDataGridViewTextBoxColumn.DataPropertyName = "stateCode";
            this.stateCodeDataGridViewTextBoxColumn.HeaderText = "stateCode";
            this.stateCodeDataGridViewTextBoxColumn.Name = "stateCodeDataGridViewTextBoxColumn";
            this.stateCodeDataGridViewTextBoxColumn.Visible = false;
            // 
            // postalCodeDataGridViewTextBoxColumn
            // 
            this.postalCodeDataGridViewTextBoxColumn.DataPropertyName = "postalCode";
            this.postalCodeDataGridViewTextBoxColumn.HeaderText = "postalCode";
            this.postalCodeDataGridViewTextBoxColumn.Name = "postalCodeDataGridViewTextBoxColumn";
            this.postalCodeDataGridViewTextBoxColumn.Visible = false;
            // 
            // countryCodeDataGridViewTextBoxColumn
            // 
            this.countryCodeDataGridViewTextBoxColumn.DataPropertyName = "countryCode";
            this.countryCodeDataGridViewTextBoxColumn.HeaderText = "countryCode";
            this.countryCodeDataGridViewTextBoxColumn.Name = "countryCodeDataGridViewTextBoxColumn";
            this.countryCodeDataGridViewTextBoxColumn.Visible = false;
            // 
            // countryNameDataGridViewTextBoxColumn
            // 
            this.countryNameDataGridViewTextBoxColumn.DataPropertyName = "countryName";
            this.countryNameDataGridViewTextBoxColumn.HeaderText = "countryName";
            this.countryNameDataGridViewTextBoxColumn.Name = "countryNameDataGridViewTextBoxColumn";
            // 
            // regionNameDataGridViewTextBoxColumn
            // 
            this.regionNameDataGridViewTextBoxColumn.DataPropertyName = "regionName";
            this.regionNameDataGridViewTextBoxColumn.HeaderText = "regionName";
            this.regionNameDataGridViewTextBoxColumn.Name = "regionNameDataGridViewTextBoxColumn";
            // 
            // timeZoneRegionNameDataGridViewTextBoxColumn
            // 
            this.timeZoneRegionNameDataGridViewTextBoxColumn.DataPropertyName = "timeZoneRegionName";
            this.timeZoneRegionNameDataGridViewTextBoxColumn.HeaderText = "timeZoneRegionName";
            this.timeZoneRegionNameDataGridViewTextBoxColumn.Name = "timeZoneRegionNameDataGridViewTextBoxColumn";
            this.timeZoneRegionNameDataGridViewTextBoxColumn.Visible = false;
            // 
            // weatherZoneDataGridViewTextBoxColumn
            // 
            this.weatherZoneDataGridViewTextBoxColumn.DataPropertyName = "weatherZone";
            this.weatherZoneDataGridViewTextBoxColumn.HeaderText = "weatherZone";
            this.weatherZoneDataGridViewTextBoxColumn.Name = "weatherZoneDataGridViewTextBoxColumn";
            this.weatherZoneDataGridViewTextBoxColumn.Visible = false;
            // 
            // localTimeDataGridViewTextBoxColumn
            // 
            this.localTimeDataGridViewTextBoxColumn.DataPropertyName = "localTime";
            this.localTimeDataGridViewTextBoxColumn.HeaderText = "localTime";
            this.localTimeDataGridViewTextBoxColumn.Name = "localTimeDataGridViewTextBoxColumn";
            this.localTimeDataGridViewTextBoxColumn.Visible = false;
            // 
            // utcOffsetHoursDataGridViewTextBoxColumn
            // 
            this.utcOffsetHoursDataGridViewTextBoxColumn.DataPropertyName = "utcOffsetHours";
            this.utcOffsetHoursDataGridViewTextBoxColumn.HeaderText = "utcOffsetHours";
            this.utcOffsetHoursDataGridViewTextBoxColumn.Name = "utcOffsetHoursDataGridViewTextBoxColumn";
            this.utcOffsetHoursDataGridViewTextBoxColumn.Visible = false;
            // 
            // latitudeDataGridViewTextBoxColumn
            // 
            this.latitudeDataGridViewTextBoxColumn.DataPropertyName = "latitude";
            this.latitudeDataGridViewTextBoxColumn.HeaderText = "latitude";
            this.latitudeDataGridViewTextBoxColumn.Name = "latitudeDataGridViewTextBoxColumn";
            this.latitudeDataGridViewTextBoxColumn.Visible = false;
            // 
            // longitudeDataGridViewTextBoxColumn
            // 
            this.longitudeDataGridViewTextBoxColumn.DataPropertyName = "longitude";
            this.longitudeDataGridViewTextBoxColumn.HeaderText = "longitude";
            this.longitudeDataGridViewTextBoxColumn.Name = "longitudeDataGridViewTextBoxColumn";
            this.longitudeDataGridViewTextBoxColumn.Visible = false;
            // 
            // elevationFeetDataGridViewTextBoxColumn
            // 
            this.elevationFeetDataGridViewTextBoxColumn.DataPropertyName = "elevationFeet";
            this.elevationFeetDataGridViewTextBoxColumn.HeaderText = "elevationFeet";
            this.elevationFeetDataGridViewTextBoxColumn.Name = "elevationFeetDataGridViewTextBoxColumn";
            this.elevationFeetDataGridViewTextBoxColumn.Visible = false;
            // 
            // elevationFeetSpecifiedDataGridViewCheckBoxColumn
            // 
            this.elevationFeetSpecifiedDataGridViewCheckBoxColumn.DataPropertyName = "elevationFeetSpecified";
            this.elevationFeetSpecifiedDataGridViewCheckBoxColumn.HeaderText = "elevationFeetSpecified";
            this.elevationFeetSpecifiedDataGridViewCheckBoxColumn.Name = "elevationFeetSpecifiedDataGridViewCheckBoxColumn";
            this.elevationFeetSpecifiedDataGridViewCheckBoxColumn.Visible = false;
            // 
            // classificationDataGridViewTextBoxColumn
            // 
            this.classificationDataGridViewTextBoxColumn.DataPropertyName = "classification";
            this.classificationDataGridViewTextBoxColumn.HeaderText = "classification";
            this.classificationDataGridViewTextBoxColumn.Name = "classificationDataGridViewTextBoxColumn";
            this.classificationDataGridViewTextBoxColumn.Visible = false;
            // 
            // activeDataGridViewCheckBoxColumn
            // 
            this.activeDataGridViewCheckBoxColumn.DataPropertyName = "active";
            this.activeDataGridViewCheckBoxColumn.HeaderText = "active";
            this.activeDataGridViewCheckBoxColumn.Name = "activeDataGridViewCheckBoxColumn";
            this.activeDataGridViewCheckBoxColumn.Visible = false;
            // 
            // dateFromDataGridViewTextBoxColumn
            // 
            this.dateFromDataGridViewTextBoxColumn.DataPropertyName = "dateFrom";
            this.dateFromDataGridViewTextBoxColumn.HeaderText = "dateFrom";
            this.dateFromDataGridViewTextBoxColumn.Name = "dateFromDataGridViewTextBoxColumn";
            this.dateFromDataGridViewTextBoxColumn.Visible = false;
            // 
            // dateToDataGridViewTextBoxColumn
            // 
            this.dateToDataGridViewTextBoxColumn.DataPropertyName = "dateTo";
            this.dateToDataGridViewTextBoxColumn.HeaderText = "dateTo";
            this.dateToDataGridViewTextBoxColumn.Name = "dateToDataGridViewTextBoxColumn";
            this.dateToDataGridViewTextBoxColumn.Visible = false;
            // 
            // delayIndexUrlDataGridViewTextBoxColumn
            // 
            this.delayIndexUrlDataGridViewTextBoxColumn.DataPropertyName = "delayIndexUrl";
            this.delayIndexUrlDataGridViewTextBoxColumn.HeaderText = "delayIndexUrl";
            this.delayIndexUrlDataGridViewTextBoxColumn.Name = "delayIndexUrlDataGridViewTextBoxColumn";
            this.delayIndexUrlDataGridViewTextBoxColumn.Visible = false;
            // 
            // weatherUrlDataGridViewTextBoxColumn
            // 
            this.weatherUrlDataGridViewTextBoxColumn.DataPropertyName = "weatherUrl";
            this.weatherUrlDataGridViewTextBoxColumn.HeaderText = "weatherUrl";
            this.weatherUrlDataGridViewTextBoxColumn.Name = "weatherUrlDataGridViewTextBoxColumn";
            this.weatherUrlDataGridViewTextBoxColumn.Visible = false;
            // 
            // airportV1BindingSource
            // 
            this.airportV1BindingSource.DataSource = typeof(Trivuelos.Region_Vuelos.airportV1);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.IndianRed;
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel5.Controls.Add(this.dgAero);
            this.panel5.Location = new System.Drawing.Point(11, 59);
            this.panel5.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(493, 204);
            this.panel5.TabIndex = 16;
            // 
            // FrmBusquedaN4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(512, 272);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbregiones);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "FrmBusquedaN4";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmBusquedaN4";
            this.Load += new System.EventHandler(this.FrmBusquedaN4_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgAero)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.airportV1BindingSource)).EndInit();
            this.panel5.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbregiones;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgAero;
        private System.Windows.Forms.BindingSource airportV1BindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn fsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn iataDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn icaoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn faaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn street1DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn street2DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cityDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cityCodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn districtDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn stateCodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn postalCodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn countryCodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn countryNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn regionNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn timeZoneRegionNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn weatherZoneDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn localTimeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn utcOffsetHoursDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn latitudeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn longitudeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn elevationFeetDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn elevationFeetSpecifiedDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn classificationDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn activeDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateFromDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateToDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn delayIndexUrlDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn weatherUrlDataGridViewTextBoxColumn;
        private System.Windows.Forms.Panel panel5;
    }
}