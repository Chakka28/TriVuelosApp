﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using Trivuelosentities;
namespace Trivuelos
{
    public partial class FrmBusquedaN1 : Form
    {
        public EUsuario Logeado;

        public FrmBusquedaN1()
        {
            InitializeComponent();
        }

        private void FrmBusqueda_Load(object sender, EventArgs e)
        {
            ptbFoto.Image = Logeado.Foto;
            lbNombre.Text = Logeado.Nombre + " " + Logeado.Apellido;
        }


        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnMaximizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Maximized;
            btnRestaurar.Visible = true;
            btnMaximizar.Visible = false;
        }

        private void btnRestaurar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Normal;
            btnRestaurar.Visible = false;
            btnMaximizar.Visible = true;
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void AbrirSEVuelo(object sender, EventArgs e)
        {
            try
            {
                lbInfomacion.Visible = true;
                lbInfomacion.Text = "Cargando... espere por favor";
                AbrirForm(new FrmSEVuelos() { Logeado = Logeado });
                lbInfomacion.Visible = false;
            }
            catch (Exception ex)
            {

                lbInfomacion.Text = ex.Message;
            }

        }

        private void AbrirNAVuelo(object sender, EventArgs e)
        {
            try
            {
                lbInfomacion.Visible = true;
                lbInfomacion.Text = "Cargando... espere por favor";
                AbrirForm(new FrmNAVuelo() { Logeado = Logeado });
                lbInfomacion.Visible = false;
            }
            catch (Exception ex)
            {

                lbInfomacion.Text = ex.Message;
            }

        }

        private void AbrirDAVuelo(object sender, EventArgs e)
        {
            try
            {
                lbInfomacion.Visible = true;
                lbInfomacion.Text = "Cargando... espere por favor";
                AbrirForm(new FrmDAVuelo() { Logeado = Logeado });
                lbInfomacion.Visible = false;
            }
            catch (Exception ex)
            {

                lbInfomacion.Text = ex.Message;
            }

        }

        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(IntPtr hWnd, int wMsg, int WParam, int lParam);

        private void Ventana(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(Handle, 0x112, 0xf012, 0);
        }
        /// <summary>
        /// Cargar un nuevo form en el panel de contol
        /// </summary>
        /// <param name="form"></param>
        private void AbrirForm(object form)
        {
            if (pContenedor.Controls.Count > 0)
            {
                pContenedor.Controls.RemoveAt(0);
            }
            Form fh = form as Form;
            fh.TopLevel = false;
            fh.Dock = DockStyle.Fill;
            pContenedor.Controls.Add(fh);
            pContenedor.Tag = fh;
            fh.Show();
        }
    }
}
