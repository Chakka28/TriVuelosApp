﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Trivuelosentities;
using TrivuelosBOL;

namespace Trivuelos
{
    public partial class FrmSEVuelos : Form
    {
        private VueloProgramadoBOL vpbol;
        private AeropuertoBOL apbol;
        public EUsuario Logeado;
        public int bandera;

        public FrmSEVuelos()
        {
            InitializeComponent();
        }

        private void FrmSEVuelos_Load(object sender, EventArgs e)
        {
            vpbol = new VueloProgramadoBOL();
            apbol = new AeropuertoBOL();
            cbxAeropuerto.DataSource = apbol.CargarDatos(Logeado);
        }

        private void BuscarLlegada(object sender, EventArgs e)
        {
            try
            {
                bandera = 2;
                lbInformacion.Text = "Cargando los vuelos, espere por favor...";
                lbInformacion.Visible = true;
                dgVuelos.DataSource = vpbol.ObtenerVuelosAeropuertoLlegada(Configuracion());
                lbInformacion.Visible = false;

            }
            catch (Exception ex)
            {
                lbInformacion.Text = ex.Message;
            }
        }


        private void BuscarSalida(object sender, EventArgs e)
        {
            try
            {
                bandera = 1;
                lbInformacion.Text = "Cargando los vuelos, espere por favor...";
                lbInformacion.Visible = true;
                dgVuelos.DataSource = vpbol.ObtenerVuelosAeropuertoSalida(Configuracion());
                lbInformacion.Visible = false;
            }
            catch (Exception ex)
            {

                lbInformacion.Text = ex.Message;
            }
        }

        /// <summary>
        /// Configuración basada en los datos datos por el usuario
        /// </summary>
        /// <returns>Configuración del usuario</returns>
        private EConfiguracionVuelo Configuracion()
        {
            EConfiguracionVuelo con = new EConfiguracionVuelo
            {
                Aeropuerto = (cbxAeropuerto.SelectedItem as EAeropuerto).Fs,
                Anno = dtpFecha.Value.Year,
                Mes = dtpFecha.Value.Month,
                Dia = dtpFecha.Value.Day,
                Hora = (int)nupHora.Value
            };
            return con;
        }

        /// <summary>
        /// Manada la configuracion de la ruta
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void VerRuta(object sender, EventArgs e)
        {
            try
            {
                Hide();
                if (bandera > 0 && dgVuelos.SelectedRows.Count > 0)
                {
                    EConfiguracionVuelo con = new EConfiguracionVuelo
                    {
                        Aeropuerto = (cbxAeropuerto.SelectedItem as EAeropuerto).Fs,
                        Anno = dtpFecha.Value.Year,
                        Mes = dtpFecha.Value.Month,
                        Dia = dtpFecha.Value.Day,
                        Hora = (int)nupHora.Value
                    };
                    EVuelo vue = (EVuelo)dgVuelos.SelectedRows[0].DataBoundItem;
                    if (bandera == 1)
                    {
                        con.Aerolinea = vue.Portador;
                        con.NumeroVuelo = vue.NumeroVuelo;
                        con.Aeropuerto = vue.LlegadaAeropuerto;
                        con.Aeropuerto2 = vue.SalidaAeropuerto;
                    }
                    else if (bandera == 2)
                    {
                        con.Aeropuerto = vue.SalidaAeropuerto;
                        con.Aeropuerto2 = vue.LlegadaAeropuerto;
                        con.Aerolinea = vue.Portador;
                        con.NumeroVuelo = vue.NumeroVuelo;
                    }
                    new FrmBusquedaN3() { vuelo = vue, configuracion = con, bandera = 1 }.ShowDialog();
                }
                else
                {
                    lbInformacion.Text = "Elija una vuelo";
                    lbInformacion.Visible = true;
                }
                Show();
            }
            catch (Exception ex)
            {

                lbInformacion.Text = ex.Message;
            }
        }
    }
}
