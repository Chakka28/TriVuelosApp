﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Trivuelos.Region_Vuelos;

namespace Trivuelos
{
    public partial class FrmBusquedaN4 : Form
    {
        public FrmBusquedaN4()
        {
            InitializeComponent();
        }

        private void FrmBusquedaN4_Load(object sender, EventArgs e)
        {

        }



        /// <summary>
        /// /Carga los aeropuertos con retrasos significativos 
        /// </summary>
        /// <param name="region">region de los aeropuertos</param>
        private void cargar(string region)
        {
            try
            {
                delayIndexServiceClient ws = new delayIndexServiceClient("delayIndexServicePort");

                responseRegion aero = ws.forRegion("ed9a785b", "d8870be5ac62b2d6ddcfb041ea4aaf3d", region, "5", "5", "");
                delayIndexV1[] arreglo = aero.delayIndexes;
                List<airportV1> aeropuertos = new List<airportV1>();
                if (arreglo.Length > 0)
                {
                    foreach (delayIndexV1 item in arreglo)
                    {
                        aeropuertos.Add(item.airport);
                    }
                    dgAero.DataSource = aeropuertos;


                }
                else
                {
                    dgAero.DataSource = aeropuertos;
                }
            }
            catch (Exception)
            {

                MessageBox.Show("Ocurrio un fallo");
            }
        }

        /// <summary>
        /// Consigue la region elegida por el cliente 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbregiones.SelectedIndex == 0)
            {
                cargar("Africa");
            }
            if (cbregiones.SelectedIndex == 1)
            {
                cargar("Antarctica");
            }
            if (cbregiones.SelectedIndex == 2)
            {
                cargar("Caribbean");
            }
            if (cbregiones.SelectedIndex == 3)
            {
                cargar("Central-America");
            }
            if (cbregiones.SelectedIndex == 4)
            {
                cargar("Europe");
            }
            if (cbregiones.SelectedIndex == 5)
            {
                cargar("Middle-East");
            }
            if (cbregiones.SelectedIndex == 6)
            {
                cargar("North-America");
            }
            if (cbregiones.SelectedIndex == 7)
            {
                cargar("Oceania");
            }
            if (cbregiones.SelectedIndex == 8)
            {
                cargar("South-America");
            }
            if (cbregiones.SelectedIndex == 9)
            {
                cargar("Asia");
            }
        }
    }
}
