﻿namespace Trivuelos
{
    partial class FrmBusquedaN2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.FechaVuelo = new System.Windows.Forms.DateTimePicker();
            this.txtNumVuelo = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cbal = new System.Windows.Forms.ComboBox();
            this.eAerolineaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblPuertaSalida = new System.Windows.Forms.Label();
            this.lblPuertaEntrada = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblTipo = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.lblEstado = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.lblTemperatura = new System.Windows.Forms.Label();
            this.lblClima = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.lblAeroLLegada = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.lblAeroSalida = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.eAerolineaBindingSource)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Trivuelos.Properties.Resources.vuelos;
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(252, 56);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.IndianRed;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.FechaVuelo);
            this.panel1.Controls.Add(this.txtNumVuelo);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.cbal);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Location = new System.Drawing.Point(12, 84);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(885, 138);
            this.panel1.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label5.Location = new System.Drawing.Point(680, 24);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 29);
            this.label5.TabIndex = 5;
            this.label5.Text = "Fecha:";
            // 
            // FechaVuelo
            // 
            this.FechaVuelo.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FechaVuelo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.FechaVuelo.Location = new System.Drawing.Point(631, 67);
            this.FechaVuelo.Margin = new System.Windows.Forms.Padding(0);
            this.FechaVuelo.Name = "FechaVuelo";
            this.FechaVuelo.Size = new System.Drawing.Size(215, 22);
            this.FechaVuelo.TabIndex = 4;
            // 
            // txtNumVuelo
            // 
            this.txtNumVuelo.Location = new System.Drawing.Point(313, 67);
            this.txtNumVuelo.Name = "txtNumVuelo";
            this.txtNumVuelo.Size = new System.Drawing.Size(215, 22);
            this.txtNumVuelo.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label4.Location = new System.Drawing.Point(308, 24);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(220, 29);
            this.label4.TabIndex = 2;
            this.label4.Text = "Numero de vuelo:";
            // 
            // cbal
            // 
            this.cbal.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.eAerolineaBindingSource, "Fs", true));
            this.cbal.DataSource = this.eAerolineaBindingSource;
            this.cbal.DisplayMember = "Nombre";
            this.cbal.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbal.FormattingEnabled = true;
            this.cbal.Location = new System.Drawing.Point(32, 65);
            this.cbal.Name = "cbal";
            this.cbal.Size = new System.Drawing.Size(193, 24);
            this.cbal.TabIndex = 1;
            this.cbal.ValueMember = "Fs";
            // 
            // eAerolineaBindingSource
            // 
            this.eAerolineaBindingSource.DataSource = typeof(Trivuelosentities.EAerolinea);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label3.Location = new System.Drawing.Point(52, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(132, 29);
            this.label3.TabIndex = 0;
            this.label3.Text = "Aerolínea:";
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.DimGray;
            this.label1.Location = new System.Drawing.Point(9, 71);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(888, 10);
            this.label1.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.DimGray;
            this.label2.Location = new System.Drawing.Point(9, 225);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(888, 10);
            this.label2.TabIndex = 9;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.IndianRed;
            this.panel2.Controls.Add(this.lblPuertaSalida);
            this.panel2.Controls.Add(this.lblPuertaEntrada);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Location = new System.Drawing.Point(12, 433);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(640, 56);
            this.panel2.TabIndex = 10;
            // 
            // lblPuertaSalida
            // 
            this.lblPuertaSalida.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPuertaSalida.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblPuertaSalida.Location = new System.Drawing.Point(561, 18);
            this.lblPuertaSalida.Name = "lblPuertaSalida";
            this.lblPuertaSalida.Size = new System.Drawing.Size(79, 29);
            this.lblPuertaSalida.TabIndex = 11;
            this.lblPuertaSalida.Text = "D/N";
            // 
            // lblPuertaEntrada
            // 
            this.lblPuertaEntrada.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPuertaEntrada.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblPuertaEntrada.Location = new System.Drawing.Point(251, 18);
            this.lblPuertaEntrada.Name = "lblPuertaEntrada";
            this.lblPuertaEntrada.Size = new System.Drawing.Size(79, 29);
            this.lblPuertaEntrada.TabIndex = 3;
            this.lblPuertaEntrada.Text = "D/N";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label7.Location = new System.Drawing.Point(327, 15);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(210, 29);
            this.label7.TabIndex = 2;
            this.label7.Text = "Puerta de salida:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label6.Location = new System.Drawing.Point(17, 15);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(228, 29);
            this.label6.TabIndex = 1;
            this.label6.Text = "Puerta de entrada:";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(730, 26);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(167, 42);
            this.button1.TabIndex = 11;
            this.button1.Text = "CARGAR";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.DimGray;
            this.label10.Location = new System.Drawing.Point(9, 420);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(888, 10);
            this.label10.TabIndex = 12;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.IndianRed;
            this.panel3.Controls.Add(this.lblTipo);
            this.panel3.Controls.Add(this.label13);
            this.panel3.Location = new System.Drawing.Point(673, 433);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(224, 56);
            this.panel3.TabIndex = 13;
            // 
            // lblTipo
            // 
            this.lblTipo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipo.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblTipo.Location = new System.Drawing.Point(108, 18);
            this.lblTipo.Name = "lblTipo";
            this.lblTipo.Size = new System.Drawing.Size(79, 29);
            this.lblTipo.TabIndex = 11;
            this.lblTipo.Text = "D/N";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label13.Location = new System.Drawing.Point(14, 15);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(74, 29);
            this.label13.TabIndex = 2;
            this.label13.Text = "Tipo:";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.IndianRed;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel4.Controls.Add(this.lblEstado);
            this.panel4.Controls.Add(this.label14);
            this.panel4.Location = new System.Drawing.Point(12, 257);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(272, 142);
            this.panel4.TabIndex = 14;
            // 
            // lblEstado
            // 
            this.lblEstado.Font = new System.Drawing.Font("Microsoft Sans Serif", 22.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstado.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblEstado.Location = new System.Drawing.Point(24, 64);
            this.lblEstado.Name = "lblEstado";
            this.lblEstado.Size = new System.Drawing.Size(211, 54);
            this.lblEstado.TabIndex = 12;
            this.lblEstado.Text = "D/N";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label14.Location = new System.Drawing.Point(15, 17);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(101, 29);
            this.label14.TabIndex = 2;
            this.label14.Text = "Estado:";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.IndianRed;
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel5.Controls.Add(this.lblTemperatura);
            this.panel5.Controls.Add(this.lblClima);
            this.panel5.Controls.Add(this.label16);
            this.panel5.Location = new System.Drawing.Point(290, 257);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(287, 142);
            this.panel5.TabIndex = 15;
            // 
            // lblTemperatura
            // 
            this.lblTemperatura.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTemperatura.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblTemperatura.Location = new System.Drawing.Point(23, 92);
            this.lblTemperatura.Name = "lblTemperatura";
            this.lblTemperatura.Size = new System.Drawing.Size(213, 26);
            this.lblTemperatura.TabIndex = 13;
            this.lblTemperatura.Text = "D/N";
            // 
            // lblClima
            // 
            this.lblClima.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblClima.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblClima.Location = new System.Drawing.Point(23, 48);
            this.lblClima.Name = "lblClima";
            this.lblClima.Size = new System.Drawing.Size(213, 26);
            this.lblClima.TabIndex = 12;
            this.lblClima.Text = "D/N";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label16.Location = new System.Drawing.Point(3, 17);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(259, 29);
            this.label16.TabIndex = 2;
            this.label16.Text = "Clima del aeropuerto";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.IndianRed;
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel6.Controls.Add(this.lblAeroLLegada);
            this.panel6.Controls.Add(this.label18);
            this.panel6.Controls.Add(this.lblAeroSalida);
            this.panel6.Controls.Add(this.label19);
            this.panel6.Controls.Add(this.label20);
            this.panel6.Location = new System.Drawing.Point(583, 238);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(314, 179);
            this.panel6.TabIndex = 16;
            // 
            // lblAeroLLegada
            // 
            this.lblAeroLLegada.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAeroLLegada.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblAeroLLegada.Location = new System.Drawing.Point(3, 134);
            this.lblAeroLLegada.Name = "lblAeroLLegada";
            this.lblAeroLLegada.Size = new System.Drawing.Size(304, 25);
            this.lblAeroLLegada.TabIndex = 16;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label18.Location = new System.Drawing.Point(3, 98);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(113, 26);
            this.label18.TabIndex = 15;
            this.label18.Text = "LLegada";
            // 
            // lblAeroSalida
            // 
            this.lblAeroSalida.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAeroSalida.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblAeroSalida.Location = new System.Drawing.Point(3, 67);
            this.lblAeroSalida.Name = "lblAeroSalida";
            this.lblAeroSalida.Size = new System.Drawing.Size(304, 25);
            this.lblAeroSalida.TabIndex = 14;
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label19.Location = new System.Drawing.Point(2, 42);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(91, 26);
            this.label19.TabIndex = 12;
            this.label19.Text = "Salida";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label20.Location = new System.Drawing.Point(3, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(156, 29);
            this.label20.TabIndex = 2;
            this.label20.Text = "Aeropuertos";
            // 
            // FrmBusquedaN2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightGray;
            this.ClientSize = new System.Drawing.Size(909, 501);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FrmBusquedaN2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmBusquedaN2";
            this.Load += new System.EventHandler(this.FrmBusquedaN2_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.eAerolineaBindingSource)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ComboBox cbal;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker FechaVuelo;
        private System.Windows.Forms.TextBox txtNumVuelo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.BindingSource eAerolineaBindingSource;
        private System.Windows.Forms.Label lblPuertaSalida;
        private System.Windows.Forms.Label lblPuertaEntrada;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label lblTipo;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label lblEstado;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label lblClima;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label lblTemperatura;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label lblAeroSalida;
        private System.Windows.Forms.Label lblAeroLLegada;
        private System.Windows.Forms.Label label18;
    }
}