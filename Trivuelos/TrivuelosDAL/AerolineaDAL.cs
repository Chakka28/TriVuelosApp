﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrivuelosDAL.WSAerolinea;
using Npgsql;
using Trivuelosentities;
namespace TrivuelosDAL
{
    public class AerolineaDAL
    {
        private airlinesServiceClient ws;

        public AerolineaDAL()
        {
            ws = new airlinesServiceClient("airlinesServicePort");
        }

        /// <summary>
        /// Actualiza las aerolineas de la base de datos
        /// </summary>
        public void ActualizarDatos()
        {
            EliminarDatos();
            airline[] aerolineas = ExtraerAPI(DatoUnico.APPID, DatoUnico.APPKEY, "");
            Insertar(aerolineas);
        }


        /// <summary>
        /// Extrae todas las aerolineas disponibles de la base de datos
        /// </summary>
        /// <returns></returns>
        public List<EAerolinea> Extraer(EUsuario u)
        {
            List<EAerolinea> aerolineas = new List<EAerolinea>();

            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                string sql = "SELECT id, fs, iata, icao, nombre, activo FROM aerolineas WHERE NOT EXISTS " +
                    " (select 1 from usu_aerolinea where usu_aerolinea.id_aero = aerolineas.fs " +
                    " and usu_aerolinea.id_usu = @id)";

                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("id",u.Id);
                NpgsqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    EAerolinea aerolinea = new EAerolinea
                    {
                        Id = reader.GetInt32(0),
                        Fs = DBNull.Value.Equals(reader.GetValue(1)) ? null : reader.GetString(1),
                        Iata = DBNull.Value.Equals(reader.GetValue(2)) ? null : reader.GetString(2),
                        Icao = DBNull.Value.Equals(reader.GetValue(3)) ? null : reader.GetString(3),
                        Nombre = reader.GetString(4),
                        Activo = reader.GetBoolean(5)
                    };
                    aerolineas.Add(aerolinea);
                }
            }
            return aerolineas;
        }


        /// <summary>
        /// Inserta todas las aerolineas extraidas de flightstats a la base de datos
        /// </summary>
        /// <param name="aerolineas">Arreglo de aerolineas</param>
        private void Insertar(airline[] aerolineas)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                string sql = "INSERT INTO aerolineas (fs, iata, icao, nombre, activo) " +
                    " VALUES (@f, @ia, @ic, @nom, @act) ";
                foreach (airline a in aerolineas)
                {
                    NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                    cmd.Parameters.AddWithValue("@f", EsVacio(a.fs));
                    cmd.Parameters.AddWithValue("@ia", EsVacio(a.iata));
                    cmd.Parameters.AddWithValue("@ic", EsVacio(a.icao));
                    cmd.Parameters.AddWithValue("@nom", a.name);
                    cmd.Parameters.AddWithValue("@act", a.active);
                    cmd.ExecuteNonQuery();
                }
            }

        }

        /// <summary>
        /// Extrae las aerolíneas actuales con el API de flightstats
        /// </summary>
        /// <param name="appId">ID del API</param>
        /// <param name="appKey">KEY del API</param>
        /// <param name="opciones">Opciones del API</param>
        /// <returns>Arreglo de Aerolineas</returns>
        private airline[] ExtraerAPI(string appId, string appKey, string opciones)
        {
            airline[] aerolineas = ws.currentAirlines(appId, appKey, opciones);
            return aerolineas;
        }

        /// <summary>
        /// Verifica si un string es null, si lo es devuelve un objeto de tipo DBNull.Value si no, devuelve el string
        /// </summary>
        /// <param name="valor">string</param>
        /// <returns>true = DBNull : string </returns>
        private object EsVacio(string valor)
        {
            if (valor == null)
            {
                return DBNull.Value;
            }
            return valor;
        }

        /// <summary>
        /// Elimina los datos de una tabla de la base de datos trivuelos
        /// </summary>
        private void EliminarDatos()
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                string sql = " DELETE FROM aerolineas ; " +
                    " ALTER SEQUENCE aerolineas_id_seq RESTART ;";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Elimina una aerolínea de un usuario con el id del usuario y el nombre de la aerolínea
        /// </summary>
        /// <param name="ae"></param>
        /// <param name="usuario"></param>
        public void EliminarAeroPorUsuario(EAerolinea ae, EUsuario usuario)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                string sql = "INSERT INTO public.usu_aerolinea(id_usu, id_aero) VALUES(@usu, @ae); ";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@usu", usuario.Id);
                cmd.Parameters.AddWithValue("@ae", ae.Fs);
                cmd.ExecuteNonQuery();
            }
        }
    }
}
