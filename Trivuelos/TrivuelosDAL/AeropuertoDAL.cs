﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrivuelosDAL.WSAeropuerto;
using Npgsql;
using Trivuelosentities;

namespace TrivuelosDAL
{
    public class AeropuertoDAL
    {
        private airportsServiceClient ws;


        public AeropuertoDAL()
        {
            ws = new airportsServiceClient("airportsServicePort");
        }

        /// <summary>
        /// Actualiza los aeropuertos de la base de datos 
        /// </summary>
        public void ActualizarDatos()
        {
            EliminarDatos();
            airport[] aeropuertos = ExtraerAPI(DatoUnico.APPID, DatoUnico.APPKEY, "");
            Insertar(aeropuertos);

        }

        /// <summary>
        /// Extrae todos los aeropuertos disponibles en la base de datos
        /// </summary>
        /// <returns>Lista de EAeropuerto</returns>
        public List<EAeropuerto> Extraer(EUsuario u)
        {
            List<EAeropuerto> aeropuertos = new List<EAeropuerto>();
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();

                string sql = "SELECT id, fs, faa, iata, icao, nombre, cuidad, codigo_estado, codigo_pais," +
                    " nombre_pais, nombre_region, tiempo_zona_region, latitud, longitud, elevacion_pie, activo," +
                    " clasificacion FROM aeropuertos WHERE NOT EXISTS  (select 1 from usu_aeropuerto where usu_aeropuerto.id_aero = aeropuertos.fs " +
                    " and usu_aeropuerto.id_usu = @id)";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("id", u.Id);
                NpgsqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    EAeropuerto aeropuerto = new EAeropuerto
                    {
                        Id = reader.GetInt32(0),
                        Fs = DBNull.Value.Equals(reader.GetValue(1)) ? null : reader.GetString(1),
                        Faa = DBNull.Value.Equals(reader.GetValue(2)) ? null : reader.GetString(2),
                        Iata = DBNull.Value.Equals(reader.GetValue(3)) ? null : reader.GetString(3),
                        Icao = DBNull.Value.Equals(reader.GetValue(4)) ? null : reader.GetString(4),
                        Nombre = reader.GetString(5),
                        Cuidad = reader.GetString(6),
                        CodigoEstado = DBNull.Value.Equals(reader.GetValue(7)) ? null : reader.GetString(7),
                        CodigoPais = reader.GetString(8),
                        NombrePais = reader.GetString(9),
                        NombreRegion = reader.GetString(10),
                        TiempoZonaRegion = reader.GetString(11),
                        Latitud = reader.GetDouble(12),
                        Longitud = reader.GetDouble(13),
                        ElevacionPie = reader.GetInt32(14),
                        Activo = reader.GetBoolean(15),
                        Clasificacion = reader.GetInt32(16)
                       
                    };
                    aeropuertos.Add(aeropuerto);
                }
            }
            return aeropuertos;
        }

        /// <summary>
        /// Inserta todos lo aeropuertos extraidos de flightstats a la base de datos
        /// </summary>
        /// <param name="aeropuertos">Arreglo de aeropuertos</param>
        private void Insertar(airport[] aeropuertos)
        {

            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();

                string sql = "INSERT INTO aeropuertos (fs, faa, iata, icao, nombre, cuidad, codigo_estado," +
                    " codigo_pais, nombre_pais, nombre_region, tiempo_zona_region, latitud," +
                    " longitud, elevacion_pie, activo, clasificacion) " +
                    "VALUES (@fs, @fa, @ia, @ic, @nom, @cui, @code, @codp, @nomp, @nomr, @tiemr, @lat, @lon, @elpie, @act, @clasi) ";

                foreach (airport a in aeropuertos)
                {

                    NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                    cmd.Parameters.AddWithValue("@fs", EsVacio(a.fs));
                    cmd.Parameters.AddWithValue("@fa", EsVacio(a.faa));
                    cmd.Parameters.AddWithValue("@ia", EsVacio(a.iata));
                    cmd.Parameters.AddWithValue("@ic", EsVacio(a.icao));
                    cmd.Parameters.AddWithValue("@nom", a.name);
                    cmd.Parameters.AddWithValue("@cui", a.city);
                    cmd.Parameters.AddWithValue("@code", EsVacio(a.stateCode));
                    cmd.Parameters.AddWithValue("@codp", a.countryCode);
                    cmd.Parameters.AddWithValue("@nomp", a.countryName);
                    cmd.Parameters.AddWithValue("@nomr", a.regionName);
                    cmd.Parameters.AddWithValue("@tiemr", a.timeZoneRegionName);
                    cmd.Parameters.AddWithValue("@lat", a.latitude);
                    cmd.Parameters.AddWithValue("@lon", a.longitude);
                    cmd.Parameters.AddWithValue("@elpie", a.elevationFeet);
                    cmd.Parameters.AddWithValue("@act", a.active);
                    cmd.Parameters.AddWithValue("@clasi", a.classification);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Extrae los aeropuertos actuales con el API de flightstats
        /// </summary>
        /// <param name="id">ID del API</param>
        /// <param name="key">KEY del API</param>
        /// <param name="opcion">opciones del API</param>
        /// <returns></returns>
        private airport[] ExtraerAPI(string id, string key, string opcion)
        {
            airport[] aeropuerto = ws.currentAirports(id, key, opcion);
            return aeropuerto;
        }

        //a.fs == null ? DBNull.Value : (object)a.fs

        /// <summary>
        /// Verifica si un string es null, si lo es devuelve un objeto de tipo DBNull.Value si no, devuelve el string
        /// </summary>
        /// <param name="valor">string</param>
        /// <returns>true = DBNull : string </returns>
        private object EsVacio(string valor)
        {
            if (valor == null)
            {
                return DBNull.Value;
            }
            return valor;
        }

        /// <summary>
        /// Elimina los datos de una tabla de la base de datos trivuelos
        /// </summary>
        private void EliminarDatos()
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                string sql = "DELETE FROM aeropuertos ;" +
                    " ALTER SEQUENCE aeropuertos_id_seq RESTART ;";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Elimina un aeropuerto de un usuario con el id del usuario y nmobre del aeropuerto
        /// </summary>
        /// <param name="aeropuerto">nombre aeropuerto</param>
        /// <param name="usuario">id usuario</param>
        public void EliminarAeroPorUsuario(EAeropuerto aeropuerto, EUsuario usuario)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                string sql = "INSERT INTO public.usu_aeropuerto(id_usu, id_aero) VALUES(@usu, @ero); ";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@usu", usuario.Id);
                cmd.Parameters.AddWithValue("@ero", aeropuerto.Fs);
                cmd.ExecuteNonQuery();
            }
        }
    }
}
