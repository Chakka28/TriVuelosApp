CREATE DATABASE trivuelos;

CREATE TABLE usuarios
(
    id serial PRIMARY KEY NOT NULL,
    nombre text NOT NULL,
    apellido text NOT NULL,
    correo text NOT NULL
);

CREATE TABLE aeropuertos
(
    id serial PRIMARY KEY NOT NULL,
    fs text,
    faa text,
	iata text,
	icao text,
    nombre text NOT NULL,
    cuidad text NOT NULL,
    codigo_estado text ,
    codigo_pais text NOT NULL,
    nombre_pais text  NOT NULL,
    nombre_region text NOT NULL,
    tiempo_zona_region text  NOT NULL,
    latitud numeric NOT NULL,
    longitud numeric NOT NULL,
    elevacion_pie integer NOT NULL,
    activo boolean NOT NULL,
    clasificacion integer NOT NULL
);

CREATE TABLE aerolineas
(
    id serial PRIMARY KEY NOT NULL,
    fs text,
    iata text,
    icao text,
    nombre text NOT NULL,
    activo boolean NOT NULL
);

CREATE TABLE usu_aeropuerto
	(
	id serial PRIMARY KEY NOT NUll,
	id_usu int NOT NULL,
	id_aero text not null
	);	


	 --esta es la llave foranea 
	--ALTER TABLE usu_aeropuerto
 --   ADD CONSTRAINT fk_usu FOREIGN KEY (id_usu)
 --   REFERENCES public.usuarios (id) MATCH SIMPLE



 CREATE TABLE usu_aerolinea
	(
	id serial PRIMARY KEY NOT NUll,
	id_usu int NOT NULL,
	id_aero text not null
	);	
	 --esta es la llave foranea 
	--ALTER TABLE usu_aerolinea
 --   ADD CONSTRAINT fk_usu FOREIGN KEY (id_usu)
 --   REFERENCES public.usuarios (id) MATCH SIMPLE
	

