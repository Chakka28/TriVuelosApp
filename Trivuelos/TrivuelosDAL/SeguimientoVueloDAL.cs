﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trivuelosentities;
using TrivuelosDAL.WSVueloSeguimiento;

namespace TrivuelosDAL
{
    public class SeguimientoVueloDAL
    {
        private flightServiceClient ws;
        private responseFlightTrackById vuelo;

        public SeguimientoVueloDAL()
        {
            ws = new flightServiceClient("flightServicePort1");

        }

        /// <summary>
        ///  Extrae los datos del vuelo por un id del API 
        /// </summary>
        /// <param name="id"></param>
        public void CargarVueloId(long id)
        {
            vuelo = ws.flightTrack_fhid(DatoUnico.APPID, DatoUnico.APPKEY, id, true, 2, 1, "raw", "");
        }

        /// <summary>
        /// Extraer un arreglo de Posiciones don el id de un vuelo usando el API de flightstats
        /// </summary>
        /// <returns></returns>
        public EPosicion[] ExtraerPosicionVuelo(long id)
        {
            vuelo = ws.flightTrack_fhid(DatoUnico.APPID, DatoUnico.APPKEY, id, true, 2, 3, "raw", "");
            return Posicion(vuelo.flightTrack.positions);
        }

        /// <summary>
        /// Extrae un arreglo de puntos con el id de un vuelo usando el API de flightstats
        /// </summary>
        /// <returns></returns>
        public EPunto[] ExtraerPuntosVuelo(long id)
        {
            vuelo = ws.flightTrack_fhid(DatoUnico.APPID, DatoUnico.APPKEY, id, true, 2, 1, "raw", "");
            return Punto(vuelo.flightTrack.waypoints);
        }

        /// <summary>
        /// Extrae los puntos de un vuelo con una fecha usando el API de flightstats
        /// </summary>
        /// <param name="con"></param>
        /// <returns>Arreglo de puntos</returns>
        public EPunto[] ExtraerPuntosVueloFecha(EConfiguracionVuelo con)
        {
            responseFlightStatus vuelos = ws.flightStatus_arr(DatoUnico.APPID, DatoUnico.APPKEY, con.Aerolinea, con.NumeroVuelo, con.Anno, con.Mes, con.Dia, "", con.Aeropuerto, "", "");
            if (vuelos.flightStatuses.Length < 1)
            {
                return null;
            }
            vuelo = ws.flightTrack_fhid(DatoUnico.APPID, DatoUnico.APPKEY, vuelos.flightStatuses[0].flightId, true, 2, 1, "raw", "") ?? null;
            return Punto(vuelo.flightTrack.waypoints);
        }

        /// <summary>
        /// Extrae las posiciones de un vuelo con una fecha usando el API de flightstats
        /// </summary>
        /// <param name="con"></param>
        /// <returns>Arreglo de posiciones</returns>
        public EPosicion[] ExtraerPosicionesVueloFecha(EConfiguracionVuelo con)
        {
            responseFlightStatus vuelos = ws.flightStatus_arr(DatoUnico.APPID, DatoUnico.APPKEY, con.Aerolinea, con.NumeroVuelo, con.Anno, con.Mes, con.Dia, "", con.Aeropuerto, "", "");
            vuelo = ws.flightTrack_fhid(DatoUnico.APPID, DatoUnico.APPKEY, vuelos.flightStatuses[0].flightId, true, 3, 1, "raw", "");
            return Posicion(vuelo.flightTrack.positions);
        }

        /// <summary>
        /// Carga la configuración de un vuelo
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public EConfiguracionVuelo ConfiguracionVuelo(long id)
        {
            vuelo = ws.flightTrack_fhid(DatoUnico.APPID, DatoUnico.APPKEY, id, true, 3, 1, "raw", "");
            EConfiguracionVuelo con = new EConfiguracionVuelo()
            {
                Aeropuerto = vuelo.flightTrack.arrivalAirportFsCode,
                Aeropuerto2 = vuelo.flightTrack.departureAirportFsCode,
                Aerolinea = vuelo.flightTrack.carrierFsCode
            };
            return con;
        }

        /// <summary>
        /// Carga los positionv2 y los convierte en Posicion para obtener los datos necesarios
        /// </summary>
        /// <param name="pos">arreglo de positions</param>
        /// <returns>Arreglo de posiciones</returns>
        private EPosicion[] Posicion(positionV2[] pos)
        {
            EPosicion[] posicion = new EPosicion[5];
            for (int i = 0; i < pos.Length; i++)
            {
                EPosicion po = new EPosicion()
                {
                    Latitud = pos[i].lat,
                    Logitud = pos[i].lon,
                    Fuente = pos[i].source,
                    Velocidad = pos[i].speedMph
                };
                posicion[i] = po;
            }
            return posicion;

        }

        /// <summary>
        /// Cargar los waypoints y los convierte en puntos
        /// </summary>
        /// <param name="pun">arreglo de waypointsv2</param>
        /// <returns>Arreglo de puntos</returns>
        private EPunto[] Punto(waypointV2[] pun)
        {
            EPunto[] punto = new EPunto[pun.Length];
            for (int i = 0; i < pun.Length; i++)
            {
                EPunto p = new EPunto()
                {
                    Lat = pun[i].lat,
                    Lon = pun[i].lon
                };
                punto[i] = p;
            }
            return punto;
        }

    }
}
