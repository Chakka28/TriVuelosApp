﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trivuelosentities;

namespace TrivuelosDAL
{
    public class UsuarioDAL
    {

        /// <summary>
        /// Inserta un nuevo usuario a la base de datos 
        /// </summary>
        /// <param name="u">el objeto de usuario</param>
        public int Insertar(EUsuario u)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                string sql = "INSERT INTO public.usuarios(nombre, apellido, correo) " +
                    " VALUES(@nom, @ap, @cor) returning id";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@nom", u.Nombre);
                cmd.Parameters.AddWithValue("@ap", u.Apellido);
                cmd.Parameters.AddWithValue("@cor", u.Correo);
            
                object id = cmd.ExecuteScalar();
                return id == DBNull.Value ? 0 : Convert.ToInt32(id);
            }
        }

        /// <summary>
        /// Metodo que verifica el usuario que se quiere logiar en la base de datos
        /// </summary>
        /// <param name="u">usuario</param>
        /// <returns>verificacion de la existencia de usuario</returns>
        public int verificar(EUsuario u)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                string sql = "SELECT id, nombre, apellido, correo FROM public.usuarios" +
                    " where correo = @cor";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@cor", u.Correo);
                NpgsqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    return reader.GetInt32(0);
                }
            }
            return 0;
        }


    }
}
