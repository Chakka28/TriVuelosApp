﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Oauth2.v2;
using Google.Apis.Oauth2.v2.Data;
using Google.Apis.Plus.v1;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using Trivuelosentities;

namespace TrivuelosDAL
{
    public class GoogleDAL
    {
        //ids de Google api- google developers
        private string nombre = "TrivuelosApp";
        private string idCliente = "908446324871-89ic1pq96lk39nr0et9dmqcpglejq97l.apps.googleusercontent.com";
        private string secretoCliente = "XPa6HdNf4Ylw6CpD8xXHccS6";
        private string[] scopes =
        {
            PlusService.Scope.PlusLogin,
            PlusService.Scope.UserinfoEmail,
            PlusService.Scope.UserinfoProfile
        };



        //const string authorizationEndpoint = "https://accounts.google.com/o/oauth2/v2/auth";

        /// <summary>
        /// Extrae lo datos del usuario registrado en Google Plus
        /// </summary>
        /// <returns>EUsuario con datos</returns>
        public EUsuario DatosUsuario()
        {
            UserCredential credencial = ObtenerCredencial();
            if (credencial != null)
            {
                Oauth2Service servicio = Servicio(credencial);
                Userinfoplus informacionUsuario = servicio.Userinfo.Get().ExecuteAsync().Result;


                EUsuario usuario = new EUsuario
                {
                    Nombre = informacionUsuario.GivenName,
                    Apellido = informacionUsuario.FamilyName,
                    Correo = informacionUsuario.Email,
                    Foto = ExtraerImagen(informacionUsuario.Picture),
                    Activo = true
                };
                Console.WriteLine(informacionUsuario.Picture);
                credencial.RevokeTokenAsync(CancellationToken.None).Wait();
                servicio.Dispose();
                return usuario;
            }
            return new EUsuario();
        }

        /// <summary>
        /// Obtiene el servicio de Google plus con la credencial de usuario
        /// </summary>
        /// <param name="credencial">Credencial</param>
        /// <returns>Servicio de Google</returns>
        private Oauth2Service Servicio(UserCredential credencial)
        {
            Oauth2Service servicio = new Oauth2Service(
               new BaseClientService.Initializer() { HttpClientInitializer = credencial });

            return servicio;
        }


        /// <summary>
        /// Crea una conexion con Google para obtener la crendencial de un usuario existente
        /// </summary>
        /// <returns>Credencial del usuario de Google</returns>
        public UserCredential ObtenerCredencial()
        {
            UserCredential credencial = null;
            try
            {
                credencial = GoogleWebAuthorizationBroker.AuthorizeAsync(
           new ClientSecrets
           {
               ClientId = idCliente,
               ClientSecret = secretoCliente
           },
           scopes,
           Environment.UserName,
           CancellationToken.None,
           new FileDataStore(nombre)).Result;
            }
            catch (Exception)
            {

                return credencial;
            }

            return credencial;
        }

        /// <summary>
        /// Extrae una URL que se convertirá en una imagen
        /// </summary>
        /// <param name="url">string url de una imagen</param>
        /// <returns>Imagen</returns>
        private Image ExtraerImagen(string url)
        {
            WebClient wc = new WebClient();
            byte[] bytes = wc.DownloadData(url);
            MemoryStream ms = new MemoryStream(bytes);
            return Image.FromStream(ms);
        }
    }
}
