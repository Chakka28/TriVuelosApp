﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrivuelosDAL.WSVueloProgramado;
using Trivuelosentities;
namespace TrivuelosDAL
{
    public class VuelosProgramadosDAL
    {
        private scheduledFlightServiceClient ws;

        public VuelosProgramadosDAL()
        {
            ws = new scheduledFlightServiceClient("scheduledFlightServicePort");
        }

        /// <summary>
        /// Extraer todo los vuelos de llegada entre dos aeropuertos
        /// </summary>
        /// <param name="con">Configuración del vuelo</param>
        /// <returns>Lista de vuelos</returns>
        public List<EVuelo> ExtraerLlegadaDosAeropuertos(EConfiguracionVuelo con)
        {
            List<EVuelo> vuelos = new List<EVuelo>();
            scheduledFlightV1[] vuelosp = ExtraerAPILlegadaDosAeropuerto(con);
            vuelos = CargarVuelo(vuelosp);
            return vuelos;
        }

        /// <summary>
        /// Extrae todo los vuelo de salida de una aerolínea
        /// </summary>
        /// <param name="con"></param>
        /// <returns>Lista de vuelos</returns>
        public List<EVuelo> ExtraerSalidaFechaAerolinea(EConfiguracionVuelo con)
        {
            List<EVuelo> vuelos = new List<EVuelo>();
            scheduledFlightV1[] vuelosp = ExtraerAPISalidaAerolinea(con);
            vuelos = CargarVuelo(vuelosp);
            return vuelos;
        }

        /// <summary>
        /// Extrae todo los vuelos de llegada de una aerolínea
        /// </summary>
        /// <param name="con"></param>
        /// <returns>Lista de vuelos</returns>
        public List<EVuelo> ExtraerLlegadaFechaAerolinea(EConfiguracionVuelo con)
        {
            List<EVuelo> vuelos = new List<EVuelo>();
            scheduledFlightV1[] vuelosp = ExtraerAPILlegadaAerolinea(con);
            vuelos = CargarVuelo(vuelosp);
            return vuelos;
        }

        /// <summary>
        /// Extrae todo los vuelos de llegada de un aeropuerto
        /// </summary>
        /// <param name="con"></param>
        /// <returns>Lista de vuelos</returns>
        public List<EVuelo> ExtraerLlegadaFechaAeropuerto(EConfiguracionVuelo con)
        {
            List<EVuelo> vuelos = new List<EVuelo>();
            scheduledFlightV1[] vuelosp = ExtraerAPILlegadaAeropuerto(con);
            vuelos = CargarVuelo(vuelosp);
            return vuelos;
        }

        /// <summary>
        /// Extrae todos lo vuelos de salida de un aeropuerto
        /// </summary>
        /// <param name="con"></param>
        /// <returns></returns>
        public List<EVuelo> ExtraerSalidaFechaAeropuerto(EConfiguracionVuelo con)
        {
            List<EVuelo> vuelos = new List<EVuelo>();
            scheduledFlightV1[] vuelosp = ExatrerAPISalidaAeropuerto(con);
            vuelos = CargarVuelo(vuelosp);
            return vuelos;
        }


        /// <summary>
        /// Extrae todos los vuelos programados de llegada de un aeropuerto en una fecha con el API de flightstats
        /// </summary>
        /// <param name="con">EConfiguracionVuelo con datos</param>
        /// <returns>arreglo de vuelos</returns>
        private scheduledFlightV1[] ExtraerAPILlegadaAeropuerto(EConfiguracionVuelo con)
        {
            scheduledFlightV1[] vuelos = ws.byAirport_Arriving(DatoUnico.APPID, DatoUnico.APPKEY, con.Aeropuerto, con.Anno, con.Mes, con.Dia, con.Hora, "", "").scheduledFlights;
            return vuelos;
        }

        /// <summary>
        /// Extrae todos los vuelos programados de salida de un aeropuerto en una fecha con el API de flightstats
        /// </summary>
        /// <param name="con"></param>
        /// <returns>Arreglo de vuelos de salida del API</returns>
        private scheduledFlightV1[] ExatrerAPISalidaAeropuerto(EConfiguracionVuelo con)
        {
            scheduledFlightV1[] vuelos = ws.byAirport_Departing(DatoUnico.APPID, DatoUnico.APPKEY, con.Aeropuerto, con.Anno, con.Mes, con.Dia, con.Hora, "", "").scheduledFlights;
            return vuelos;
        }

        /// <summary>
        /// Extrae los todos vuelos programados de llegada de un aeropuerto en una fecha con el API de flightstats
        /// </summary>
        /// <param name="con"></param>
        /// <returns>Arreglo de vuelos de Llegada del API</returns>
        private scheduledFlightV1[] ExtraerAPILlegadaAerolinea(EConfiguracionVuelo con)
        {
            scheduledFlightV1[] vuelos = ws.byFlight_Arriving(DatoUnico.APPID, DatoUnico.APPKEY, con.Aerolinea, con.NumeroVuelo, con.Anno, con.Mes, con.Dia, "", "").scheduledFlights;
            return vuelos;
        }

        /// <summary>
        /// Extrae los todos vuelos programados de salida de una aerolínea en una fecha con el API de flightstats
        /// </summary>
        /// <param name="con"></param>
        /// <returns>Arreglo de vuelos de salida del API</returns>
        private scheduledFlightV1[] ExtraerAPISalidaAerolinea(EConfiguracionVuelo con)
        {
            scheduledFlightV1[] vuelos = ws.byFlight_Departing(DatoUnico.APPID, DatoUnico.APPKEY, con.Aerolinea, con.NumeroVuelo, con.Anno, con.Mes, con.Dia, "", "").scheduledFlights;
            return vuelos;
        }

        /// <summary>
        /// Extrae todos los vuelos programados en una fecha entre dos aeropuertos con el API de flightstats
        /// </summary>
        /// <param name="con"></param>
        /// <returns>Arreglo de vuelos de Llegada del API</returns>
        private scheduledFlightV1[] ExtraerAPILlegadaDosAeropuerto(EConfiguracionVuelo con)
        {
            scheduledFlightV1[] vuelos = ws.byRoute_Arriving(DatoUnico.APPID, DatoUnico.APPKEY, con.Aerolinea, con.Aeropuerto2, con.Anno, con.Mes, con.Dia, "", "").scheduledFlights;
            return vuelos;
        }

        /// <summary>
        /// Recorre un arreglo de vuelos programados para retornar una lista de vuelos con los datos necesarios
        /// </summary>
        /// <param name="vuelosp">Arreglo de vuelos del API</param>
        /// <returns>Lista de vuelos</returns>
        private List<EVuelo> CargarVuelo(scheduledFlightV1[] vuelosp)
        {
            List<EVuelo> vuelos = new List<EVuelo>();
            foreach (scheduledFlightV1 v in vuelosp)
            {
                EVuelo vuelo = new EVuelo
                {
                    LlegadaAeropuerto = v.arrivalAirportFsCode,
                    SalidaAeropuerto = v.departureAirportFsCode,
                    HoraLlegada = v.arrivalTime,
                    HoraSalida = v.departureTime,
                    NumeroVuelo = v.flightNumber,
                    Parada = v.stops,
                    Portador = v.carrierFsCode,
                    VueloEquipo = v.flightEquipmentIataCode
                };
                vuelos.Add(vuelo);
            }
            return vuelos;
        }
    }
}
